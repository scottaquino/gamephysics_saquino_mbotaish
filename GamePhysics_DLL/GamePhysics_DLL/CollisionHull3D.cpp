#include "CollisionHull3D.h"

bool CollisionHull3D::TestCollision(CollisionHull3D* b, Collision& c)
{
	if (b->type == CollisionHullType3D::SPHERE) { return this->TestSphere(b, c);}
	else if (b->type == CollisionHullType3D::AABB) { return this->TestAxisBox(b, c);}
	else if (b->type == CollisionHullType3D::OBB) { return this->TestObjectBox(b, c);}

	return false;
}

bool CollisionHull3D::Vector3Comparison(Vector3 left, Vector3 right)
{
	if (left.data.x >= right.data.x && left.data.y >= right.data.y && left.data.z >= right.data.z)
	{
		return true;
	}
	return false;
}


