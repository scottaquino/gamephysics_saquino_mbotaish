#include "MyUnityPlugin.h"


bool TestCollision(int first, int second)
{
	return Physics::getInstance()->TestCollision(first, second);
}

int CreateCircle(float radius)
{
	return Physics::getInstance()->CreateBounds(radius);
}


int CreateBox(float x, float y, float z, bool canRotate)
{
	return Physics::getInstance()->CreateBounds(x,y,z, canRotate);
}

void SetPosition(float x, float y, float z, int index)
{
	Physics::getInstance()->SetPosition(x, y, z, index);
}

void SetPositionWithRotation(float x, float y, float z, int type, int index)
{
	Physics::getInstance()->SetPositionWithRotation(x, y, z, type, index);
}

void SetPositionOBBSphere(float x, float y, float z, int index)
{
	Physics::getInstance()->SetPositionOBBSphere(x, y, z, index);
}

float GetPositionX(int index)
{
	return Physics::getInstance()->GetPositionX(index);
}

void CleanPlugin()
{
	Physics::getInstance()->Clean();
}

