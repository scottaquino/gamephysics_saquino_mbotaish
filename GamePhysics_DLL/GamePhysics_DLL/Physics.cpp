#include "Physics.h"
#include <string>
#include <iostream>

Physics* Physics::mInstance = NULL;

int Physics::CreateBounds(float radius)
{
	int index = mCollisionObjects.size();

	mCollisionObjects.push_back(new SphereHull3D(radius));

	return index;
}

int Physics::CreateBounds(float x, float y, float z, bool canRotate)
{
	int index = mCollisionObjects.size();

	Vector3 dim = Vector3(x, y, z);

	if (canRotate) { mCollisionObjects.push_back(new ObjectBoxHull3D(dim)); }
	else { mCollisionObjects.push_back(new AxisAlignHull3D(dim)); }
	
	return index;
}

void Physics::SetPosition(float x, float y, float z, int index)
{
	mCollisionObjects[index]->position = Vector3(x, y, z);
}

void Physics::SetPositionWithRotation(float x, float y, float z, int type, int index)
{
	ObjectBoxHull3D* box = static_cast<ObjectBoxHull3D*>(mCollisionObjects[index]);
	//std::cout << type << "";
	switch (type)
	{
	case 0:		
		box->verticies[type] = Vector3(x, y, z);
		break;
	case 1:
		box->verticies[type] = Vector3(x, y, z);
		break;
	case 2:
		box->verticies[type] = Vector3(x, y, z);
		break;
	case 3:
		box->verticies[type] = Vector3(x, y, z);
		break;
	case 4:
		box->verticies[type] = Vector3(x, y, z);
		break;
	case 5:
		box->verticies[type] = Vector3(x, y, z);
		break;
	case 6:
		box->verticies[type] = Vector3(x, y, z);
		break;
	case 7:		
		box->verticies[type] = Vector3(x, y, z);
		break;
	case 8:
		box->rightAxis = Vector3(x, y, z);
		break;
	case 9:
		box->upAxis = Vector3(x, y, z);
		break;
	case 10:
		box->forwardAxis = Vector3(x, y, z);
		break;
	default:
		break;
	}

}

void Physics::SetPositionOBBSphere(float x, float y, float z, int index)
{
	SphereHull3D* sphere = static_cast<SphereHull3D*>(mCollisionObjects[index]);
	sphere->obbPostion = Vector3(x, y, z);
}

bool Physics::TestCollision(int first, int second)
{
	Collision col;	
	return mCollisionObjects[first]->TestCollision(mCollisionObjects[second], col);;
}

Physics::~Physics()
{
	for (auto collider : mCollisionObjects) {
		delete collider;
	}
	
	mCollisionObjects.clear();
}
