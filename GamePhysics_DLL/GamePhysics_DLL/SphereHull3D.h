#ifndef SPHEREHULL3D_H
#define SPHEREHULL3D_H

#include "CollisionHull3D.h"
#include <iostream>
class SphereHull3D : public CollisionHull3D
{
public:
	SphereHull3D(float theRadius) :radius(theRadius) { SetType(CollisionHullType3D::SPHERE); position = Vector3(0, 0, 0); }
	SphereHull3D() { radius = 1; SetType(CollisionHullType3D::SPHERE);position = Vector3(0, 0, 0);}
	~SphereHull3D() { }

	bool TestSphere(CollisionHull3D* other, Collision& c);
	bool TestAxisBox(CollisionHull3D* other, Collision& c);
	bool TestObjectBox(CollisionHull3D* other, Collision& c);

	float radius;

	Vector3 obbPostion;

};

#endif // !SPHEREHULL3D_H