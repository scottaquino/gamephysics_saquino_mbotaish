#include "Vector3.h"

std::string ToString(Vector3 vector)
{
	return  std::to_string(vector.GetX()) + ", " + std::to_string(vector.GetY()) + ", " + std::to_string(vector.GetZ());
}

float Dot(Vector3 const& left, Vector3 const& right) { return (left.data.x * right.data.x) + (left.data.y * right.data.y) + (left.data.z * right.data.z); }

Vector3 Cross(const Vector3& left, const Vector3& right)
{
	float x = (left.data.y * right.data.z) - (left.data.z * right.data.y);
	float y = (left.data.z * right.data.x) - (left.data.x * right.data.z);
	float z = (left.data.x * right.data.y) - (left.data.y * right.data.x);
	return Vector3(x,y,z);
}

Vector3 Normalize(Vector3 vect)
{
	float length = std::sqrt(Dot(vect, vect));

	float x = vect.data.x / length;
	float y = vect.data.y / length;
	float z = vect.data.z / length;
	
	return Vector3(x, y, z);;
}

float SquareMagnitude(Vector3 vect)
{
	return (vect.data.x * vect.data.x + vect.data.y * vect.data.y + vect.data.z * vect.data.z);
}

bool OverloadedEqualEqualTest(Vector3 lhs, Vector3 rhs)
{
	if (lhs.data.x != rhs.data.x)
	{
		return false;
	}
	if (lhs.data.y != rhs.data.y)
	{
		return false;
	}
	if (lhs.data.z != rhs.data.z)
	{
		return false;
	}

		return true;
}

Vector3::Vector3(float x, float y, float z) 
{ 
	data.x = x;
	data.y = y;
	data.z = z;
}

Vector3::Vector3(Data thisData)
{
	data = thisData;
}

Vector3::Vector3()
{
	data.x = 0;
	data.y = 0;
	data.z = 0;
}

