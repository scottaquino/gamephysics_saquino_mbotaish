#include "SphereHull3D.h"
#include "AxisAlignHull3D.h"
#include "ObjectBoxHull3D.h"


ObjectBoxHull3D::ObjectBoxHull3D(Vector3 theDimensions)
{
	SetType(CollisionHullType3D::OBB);
	half_dimensions = theDimensions * 0.5f;
}

bool ObjectBoxHull3D::TestSphere(CollisionHull3D* b, Collision& c)
{
	SphereHull3D* other = static_cast<SphereHull3D*>(b);
	return other->TestObjectBox(this, c);
}

bool ObjectBoxHull3D::TestAxisBox(CollisionHull3D* b, Collision& c)
{
	AxisAlignHull3D* other = static_cast<AxisAlignHull3D*>(b);
	return other->TestObjectBox(this, c);;
}

bool ObjectBoxHull3D::TestObjectBox(CollisionHull3D* b, Collision& c)
{
	ObjectBoxHull3D* other = static_cast<ObjectBoxHull3D*>(b);

	Vector3 temp;
	axis[0] = rightAxis;
	axis[1] = upAxis;
	axis[2] = forwardAxis;
	axis[3] = other->rightAxis;
	axis[4] = other->upAxis;
	axis[5] = other->forwardAxis;
	axis[6] = Cross(rightAxis, other->rightAxis);
	axis[7] = Cross(rightAxis, other->upAxis);
	axis[8] = Cross(rightAxis, other->forwardAxis);
	axis[9] = Cross(upAxis, other->rightAxis);
	axis[10] = Cross(upAxis, other->upAxis);
	axis[11] = Cross(upAxis, other->forwardAxis);
	axis[12] = Cross(forwardAxis, other->rightAxis);
	axis[13] = Cross(forwardAxis, other->upAxis);
	axis[14] = Cross(forwardAxis, other->forwardAxis);


	for (int x = 0; x < 15; ++x)
	{
		if (OverloadedEqualEqualTest(axis[x], temp.zero))
		{
			continue;
		}

		float minOBB1 = std::numeric_limits<float>::max();
		float minOBB2 = std::numeric_limits<float>::max();
		float maxOBB1 = -std::numeric_limits<float>::max();
		float maxOBB2 = -std::numeric_limits<float>::max();

		for (int i = 0; i < 8; ++i)
		{
			float obbDistance1 = Dot(verticies[i], axis[x]);

			if (obbDistance1 < minOBB1) { minOBB1 = obbDistance1; }
			if (obbDistance1 > maxOBB1) { maxOBB1 = obbDistance1; }

			float obbDistance2 = Dot(other->verticies[i], axis[x]);


			if (obbDistance2 < minOBB2) { minOBB2 = obbDistance2; }
			if (obbDistance2 > maxOBB2) { maxOBB2 = obbDistance2; }

		}


		float distance = std::fmax(maxOBB1, maxOBB2) - std::fmin(minOBB1, minOBB2);
		float spanDistance = (maxOBB1 - minOBB1) + (maxOBB2 - minOBB2);

		if (distance >= spanDistance) { return false; }
	}

	return true;
}


