#ifndef OBJECTBOXHULL3D_H
#define OBJECTBOXHULL3D_H

#include "CollisionHull3D.h"



class ObjectBoxHull3D : public CollisionHull3D
{
public:
	ObjectBoxHull3D(Vector3 theDimensions);
	ObjectBoxHull3D() { SetType(CollisionHullType3D::OBB); }
	~ObjectBoxHull3D() { }

	bool TestSphere(CollisionHull3D* other, Collision& c);
	bool TestAxisBox(CollisionHull3D* other, Collision& c);
	bool TestObjectBox(CollisionHull3D* other, Collision& c);

	Vector3 invTransformPos;
	Vector3 half_dimensions;

	Vector3 rightAxis, upAxis, forwardAxis;

	Vector3 axis[15];
	Vector3 verticies[8];
};

#endif // !OBJECTBOXHULL3D_H