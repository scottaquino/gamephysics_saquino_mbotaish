#ifndef MYUNITYPLUGIN_H
#define MYUNITYPLUGIN_H

#include "lib.h"
#include "Vector3.h"
#include "Physics.h"

#ifdef __cplusplus

extern "C"
{

#else // !__cplusplus 

#endif // __cplusplus


MYUNITYPLUGIN_SYMBOL bool TestCollision(int first, int second);

MYUNITYPLUGIN_SYMBOL int CreateCircle(float radius);
MYUNITYPLUGIN_SYMBOL int CreateBox(float x, float y, float z, bool canRotate);
MYUNITYPLUGIN_SYMBOL void SetPosition(float x, float y, float z, int index);
MYUNITYPLUGIN_SYMBOL void SetPositionWithRotation(float x, float y, float z, int type, int index);
MYUNITYPLUGIN_SYMBOL void SetPositionOBBSphere(float x, float y, float z, int index);


MYUNITYPLUGIN_SYMBOL float GetPositionX(int index);

MYUNITYPLUGIN_SYMBOL void CleanPlugin();

#ifdef __cplusplus

}


#else // !__cplusplus 

#endif // __cplusplus 


#endif // !MYUNITYPLUGIN_H
