#ifndef AXISALIGNHULL3D_H
#define AXISALIGNHULL3D_H

#include "CollisionHull3D.h"

class AxisAlignHull3D : public CollisionHull3D
{
public:
	AxisAlignHull3D(Vector3 theDimensions);
	AxisAlignHull3D() { SetType(CollisionHullType3D::AABB); }
	~AxisAlignHull3D() { }

	bool TestSphere(CollisionHull3D* other, Collision& c);
	bool TestAxisBox(CollisionHull3D* other, Collision& c);
	bool TestObjectBox(CollisionHull3D* other, Collision& c);

	Vector3 half_dimensions;

	Vector3 dimensions;

	Vector3 axis[15];
	Vector3 verticies[8];
};


#endif // !AXISALIGNHULL3D_H