#ifndef PHYSICS_H
#define PHYSICS_H

#include <vector>
#include "CollisionHull3D.h"
#include <windows.h>
#include "SphereHull3D.h"
#include "AxisAlignHull3D.h"
#include "ObjectBoxHull3D.h"

class Physics {
public:
	static Physics* getInstance() {
		if (mInstance == nullptr) {
			mInstance = new Physics;
		}
		AllocConsole();
		freopen("CONOUT$", "w", stdout);
		return mInstance;
	}

	static void Clean() {
		delete mInstance;
		mInstance = nullptr;
	}

	int CreateBounds(float radius);
	int CreateBounds(float x, float y, float z, bool canRotate);
	void SetPosition(float x, float y, float z, int index);
	void SetPositionWithRotation(float x, float y, float z, int type, int index);
	void SetPositionOBBSphere(float x, float y, float z, int index);
	bool TestCollision(int first, int second);

	float GetPositionX(int index) { return mCollisionObjects[index]->position.GetX(); }
	
private:
	Physics() {}
	~Physics();

	static Physics* mInstance;
	std::vector<CollisionHull3D* > mCollisionObjects;
};


#endif // !PHYSICS_H