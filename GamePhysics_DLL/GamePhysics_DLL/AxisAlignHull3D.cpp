#include "SphereHull3D.h"
#include "AxisAlignHull3D.h"
#include "ObjectBoxHull3D.h"

AxisAlignHull3D::AxisAlignHull3D(Vector3 theDimensions)
	:dimensions(theDimensions)
{
	SetType(CollisionHullType3D::AABB);
	half_dimensions = dimensions * 0.5f;
	std::cout << ToString(half_dimensions) << "--------\n";

	verticies[0] = Vector3(-half_dimensions.GetX(), half_dimensions.GetY(), half_dimensions.GetZ());
	verticies[1] = Vector3(half_dimensions.GetX(), half_dimensions.GetY(), half_dimensions.GetZ());
	verticies[2] = Vector3(-half_dimensions.GetX(), -half_dimensions.GetY(), half_dimensions.GetZ());
	verticies[3] = Vector3(half_dimensions.GetX(), -half_dimensions.GetY(), half_dimensions.GetZ());

	verticies[4] = Vector3(-half_dimensions.GetX(), half_dimensions.GetY(), -half_dimensions.GetZ());
	verticies[5] = Vector3(half_dimensions.GetX(), half_dimensions.GetY(), -half_dimensions.GetZ());
	verticies[6] = Vector3(-half_dimensions.GetX(), -half_dimensions.GetY(), -half_dimensions.GetZ());
	verticies[7] = Vector3(half_dimensions.GetX(), -half_dimensions.GetY(), -half_dimensions.GetZ());

}

bool AxisAlignHull3D::TestSphere(CollisionHull3D* b, Collision& c)
{
	SphereHull3D* other = static_cast<SphereHull3D*>(b);
	return other->TestAxisBox(this, c);
}

bool AxisAlignHull3D::TestAxisBox(CollisionHull3D* b, Collision& c)
{

	AxisAlignHull3D* other = static_cast<AxisAlignHull3D*>(b);

	Vector3 dimensoinsOther = other->dimensions * 0.5f;

	float maxX = position.data.x + half_dimensions.data.x;
	float minX = position.data.x - half_dimensions.data.x;
	float maxY = position.data.y + half_dimensions.data.y;
	float minY = position.data.y - half_dimensions.data.y;
	float maxZ = position.data.z + half_dimensions.data.z;
	float minZ = position.data.z - half_dimensions.data.z;

	float minXOther = other->position.data.x - dimensoinsOther.data.x;
	float maxXOther = other->position.data.x + dimensoinsOther.data.x;
	float minYOther = other->position.data.y - dimensoinsOther.data.y;
	float maxYOther = other->position.data.y + dimensoinsOther.data.y;
	float minZOther = other->position.data.z - dimensoinsOther.data.z;
	float maxZOther = other->position.data.z + dimensoinsOther.data.z;



	if ((maxX >= minXOther && minX <= maxXOther) &&
		(maxY >= minYOther && minY <= maxYOther) &&
		(maxZ >= minZOther && minZ <= maxZOther))
	{

		
		  //  1) Get the closest contact point
		  //  2) Get the normal of collision
		  //  3) set the restitution of each object
		  //  4) Set the contact count
		  //  5) Set the separating velocity
		  //  6) Get the penetration depth
		 

		float clampedX = std::clamp(position.data.x, minXOther, maxXOther);
		float clampedY = std::clamp(position.data.y, minYOther, maxYOther);

		Vector3 contactPoint(clampedX, clampedY, 0);
		Vector3 surfaceNormal = Normalize(position - contactPoint);

		return true;
	}


	return false;
}

bool AxisAlignHull3D::TestObjectBox(CollisionHull3D* b, Collision& c)
{
	ObjectBoxHull3D* other = static_cast<ObjectBoxHull3D*>(b);

	Vector3 temp;
	axis[0] = temp.right;
	axis[1] = temp.up;
	axis[2] = temp.forward;
	axis[3] = other->rightAxis;
	axis[4] = other->upAxis;
	axis[5] = other->forwardAxis;
	axis[6] = Cross(temp.right, other->rightAxis);
	axis[7] = Cross(temp.right, other->upAxis);
	axis[8] = Cross(temp.right, other->forwardAxis);
	axis[9] = Cross(temp.up, other->rightAxis);
	axis[10] = Cross(temp.up, other->upAxis);
	axis[11] = Cross(temp.up, other->forwardAxis);
	axis[12] = Cross(temp.forward, other->rightAxis);
	axis[13] = Cross(temp.forward, other->upAxis);
	axis[14] = Cross(temp.forward, other->forwardAxis);


	for (int x = 0; x < 15; ++x)
	{
		if (OverloadedEqualEqualTest(axis[x], temp.zero))
		{
			continue;
		}

		float minOBB1 = std::numeric_limits<float>::max();
		float minOBB2 = std::numeric_limits<float>::max();
		float maxOBB1 = -std::numeric_limits<float>::max();
		float maxOBB2 = -std::numeric_limits<float>::max();

		for (int i = 0; i < 8; ++i)
		{
			float obbDistance1 = Dot(verticies[i] + position, axis[x]);
			
			
			
			if (obbDistance1 < minOBB1) { minOBB1 = obbDistance1; }
			if (obbDistance1 > maxOBB1) { maxOBB1 = obbDistance1; }

			float obbDistance2 = Dot(other->verticies[i], axis[x]);
		

			if (obbDistance2 < minOBB2) { minOBB2 = obbDistance2; }
			if (obbDistance2 > maxOBB2) { maxOBB2 = obbDistance2; }

		}


		float distance = std::fmax(maxOBB1, maxOBB2) - std::fmin(minOBB1, minOBB2);
		float spanDistance = (maxOBB1 - minOBB1) + (maxOBB2 - minOBB2);
	
		if (distance >= spanDistance) { return false; }
	}

	return true;
}
