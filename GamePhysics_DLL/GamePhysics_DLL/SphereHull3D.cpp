#include "SphereHull3D.h"
#include "AxisAlignHull3D.h"
#include "ObjectBoxHull3D.h"

bool SphereHull3D::TestSphere(CollisionHull3D* b, Collision& c)
{

	SphereHull3D* other = static_cast<SphereHull3D*>(b);

	std::cout << "HELLO?\n";

	Vector3 distance = Vector3(other->position - position);
	
	float radiiSum = other->radius + radius;
	if (Dot(distance, distance) <= radiiSum * radiiSum)
	{
		return true;
	}

	return false;
}

bool SphereHull3D::TestAxisBox(CollisionHull3D* b, Collision& c)
{
	AxisAlignHull3D* other = static_cast<AxisAlignHull3D*>(b);

	Vector3 relCenter = position - other->position;

	Vector3 closestPoint;

	float dist = relCenter.data.x;
	if (dist > other->half_dimensions.data.x) dist = other->half_dimensions.data.x;
	if (dist < -other->half_dimensions.data.x) dist = -other->half_dimensions.data.x;
	closestPoint.data.x = dist;

	dist = relCenter.data.y;
	if (dist > other->half_dimensions.data.y) dist = other->half_dimensions.data.y;
	if (dist < -other->half_dimensions.data.y) dist = -other->half_dimensions.data.y;
	closestPoint.data.y = dist;

	dist = relCenter.data.z;
	if (dist > other->half_dimensions.data.z) dist = other->half_dimensions.data.z;
	if (dist < -other->half_dimensions.data.z) dist = -other->half_dimensions.data.z;
	closestPoint.data.z = dist;


	dist = SquareMagnitude(closestPoint - relCenter);

	if (!(dist > radius * radius))
	{
		return true;
	}

	return false;
}

bool SphereHull3D::TestObjectBox(CollisionHull3D* b, Collision& c)
{
	ObjectBoxHull3D* other = static_cast<ObjectBoxHull3D*>(b);

	Vector3 relCenter = obbPostion;

	Vector3 closestPoint;

	float dist = relCenter.data.x;
	if (dist > other->half_dimensions.data.x) dist = other->half_dimensions.data.x;
	if (dist < -other->half_dimensions.data.x) dist = -other->half_dimensions.data.x;
	closestPoint.data.x = dist;

	dist = relCenter.data.y;
	if (dist > other->half_dimensions.data.y) dist = other->half_dimensions.data.y;
	if (dist < -other->half_dimensions.data.y) dist = -other->half_dimensions.data.y;
	closestPoint.data.y = dist;

	dist = relCenter.data.z;
	if (dist > other->half_dimensions.data.z) dist = other->half_dimensions.data.z;
	if (dist < -other->half_dimensions.data.z) dist = -other->half_dimensions.data.z;
	closestPoint.data.z = dist;


	dist = SquareMagnitude(closestPoint - relCenter);

	if (!(dist > radius * radius))
	{
		return true;
	}

	return false;
}


