#ifndef COLLISIONHULL3D_H
#define COLLISIONHULL3D_H

#include "Vector3.h"
#include <algorithm>
#include <limits>

enum class CollisionHullType3D
{
	SPHERE,
	AABB,
	OBB
};

struct Collision
{
	struct Contact
	{
		Vector3 point;
		Vector3 normal;
	};

	float restitutionA, restitutionB;
	//CollisionHull3D a, b;
	int contactCount = 0;
	bool status = false;

	float separatingVelocity;
	float penetration;
};

class CollisionHull3D
{
public:
	CollisionHull3D() { type = CollisionHullType3D::SPHERE; };
	virtual ~CollisionHull3D() {};

	CollisionHullType3D GetType() { return type; }
	void SetType(CollisionHullType3D type_set) { type = type_set; }
	
	static bool Vector3Comparison(Vector3 left, Vector3 right);

	virtual bool TestCollision(CollisionHull3D* b, Collision& c);

	virtual bool TestSphere(CollisionHull3D* other, Collision& c) { return false; };
	virtual bool TestAxisBox(CollisionHull3D* other, Collision& c) { return false; };
	virtual bool TestObjectBox(CollisionHull3D* other, Collision& c) { return false; };

	virtual void UpdatePosition(Vector3 pos) { position = pos; }

	Vector3 position;

private:
	CollisionHullType3D type;
};


#endif // !COLLISION_HULL_3D_H