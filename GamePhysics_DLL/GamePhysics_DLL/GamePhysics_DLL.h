// The following ifdef block is the standard way of creating macros which make exporting
// from a DLL simpler. All files within this DLL are compiled with the GAMEPHYSICSDLL_EXPORTS
// symbol defined on the command line. This symbol should not be defined on any project
// that uses this DLL. This way any other project whose source files include this file see
// GAMEPHYSICSDLL_API functions as being imported from a DLL, whereas this DLL sees symbols
// defined with this macro as being exported.
#ifdef GAMEPHYSICSDLL_EXPORTS
#define GAMEPHYSICSDLL_API __declspec(dllexport)
#else
#define GAMEPHYSICSDLL_API __declspec(dllimport)
#endif

// This class is exported from the dll
class GAMEPHYSICSDLL_API CGamePhysicsDLL {
public:
	CGamePhysicsDLL(void);
	// TODO: add your methods here.
};

extern GAMEPHYSICSDLL_API int nGamePhysicsDLL;

GAMEPHYSICSDLL_API int fnGamePhysicsDLL(void);
