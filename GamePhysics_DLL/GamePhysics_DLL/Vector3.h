#pragma once

#include <string>
#include <iostream>

struct Data {
	float x;
	float y;
	float z;

	Data() : x(0), y(0), z(0) {}
	Data(float x, float y, float z) : x(x), y(y), z(z) {}
	~Data() {}

};


class Vector3 {
public:
	Vector3(float x, float y, float z);
	Vector3(Data data);
	Vector3();
	~Vector3() {}

	Data data;
	Data zero = Data(0, 0, 0);
	Data right = Data(1, 0, 0);
	Data forward = Data(0, 0, 1);
	Data up = Data(0, 1, 0);

	float GetX() { return data.x; }
	float GetY() { return data.y; }
	float GetZ() { return data.z; }

	inline Vector3& operator/(const float div) {
		return *new Vector3(data.x / div, data.y / div, data.z / div);
	}
	inline Vector3& operator*(const float div) {
		return *new Vector3(data.x * div, data.y * div, data.z * div);
	}

	inline Vector3& operator-(const Vector3 right) {
		return *new Vector3(data.x - right.data.x, data.y - right.data.y, data.z - right.data.z);
	}

	inline Vector3& operator+(const Vector3 right) {
		return *new Vector3(data.x + right.data.x, data.y + right.data.y, data.z + right.data.z);
	}

	inline void operator=(const Vector3 right) {
		data.x = right.data.x;
		data.y = right.data.y;
		data.z = right.data.z;
	}

	inline bool operator==(const Vector3 right) {
		return	(data.x == right.data.x) && 
				(data.y == right.data.y) &&
				(data.z == right.data.z);
	}

private:

};

std::string ToString(Vector3 vector);
float Dot(Vector3 const& left, Vector3 const& right);
Vector3 Cross(const Vector3& left, const Vector3& right);
Vector3 Normalize(Vector3 vect);
float SquareMagnitude(Vector3 vect);
bool OverloadedEqualEqualTest(Vector3 lhs, Vector3 rhs);
