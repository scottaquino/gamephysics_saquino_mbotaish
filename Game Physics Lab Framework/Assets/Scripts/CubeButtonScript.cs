﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeButtonScript : MonoBehaviour
{
    public GameObject cubePrefab;
    public GameObject _spawnedCube;
    public CubeManagerScript cmScript;

    private void OnMouseDown()
    {
        _spawnedCube = Instantiate(cubePrefab, transform.position, Quaternion.identity);
        cmScript.SetForceOption(_spawnedCube);
        cmScript.SetResistanceOption(_spawnedCube);
        cmScript.SetForceTypeOption(_spawnedCube);
        cmScript.SetSettings(_spawnedCube);
    }

    private void OnMouseUp()
    {
        _spawnedCube.GetComponent<Particle3D>().enabled = true;
        _spawnedCube = null;        
    }

    private void Update()
    {
        Vector3 mouse = Input.mousePosition;
        if (_spawnedCube)
        {
            mouse.z = 10;
            _spawnedCube.transform.position = Camera.main.ScreenToWorldPoint(mouse);
        }
    }
}
