﻿using System.Collections;
using System.Collections.Generic;
//using UnityEngine;

using System.Runtime.InteropServices;

public class MyUnityPlugin
{
    [DllImport("GAMEPHYSICSDLL")]
    public static extern int IntFoo(int f_new = 0);

    [DllImport("GAMEPHYSICSDLL")]
    public static extern int DoFoo(int bar = 0);

    [DllImport("GAMEPHYSICSDLL")]
    public static extern int TermFoo();
}
