﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.InteropServices;

public class PhysicsPlugin : MonoBehaviour
{
    public static PhysicsPlugin instance;

    #region PLUGIN
    [DllImport("GAMEPHYSICSDLL")] public static extern int CreateCircle(float radius);
    [DllImport("GAMEPHYSICSDLL")] public static extern int CreateBox(float x, float y, float z, bool canRotate);
    [DllImport("GAMEPHYSICSDLL")] public static extern void CleanPlugin();
    [DllImport("GAMEPHYSICSDLL")] public static extern void SetPosition(float x, float y, float z, int index);
    [DllImport("GAMEPHYSICSDLL")] public static extern bool TestCollision(int first, int second);
    [DllImport("GAMEPHYSICSDLL")] public static extern float GetPositionX(int index);
    [DllImport("GAMEPHYSICSDLL")] public static extern void SetPositionWithRotation(float x, float y, float z, int type, int index);
    [DllImport("GAMEPHYSICSDLL")] public static extern void SetPositionOBBSphere(float x, float y, float z, int index);
    #endregion

    public GameObject Circle;
    public GameObject BoxOne;
    public GameObject BoxTwo;

    public Material collisionMat;
    public Material NormalMat;

    private List<GameObject> objs = new List<GameObject>();

    // Start is called before the first frame update
    void Awake()
    {
        instance = this;
        CreateCircle(0.5f);
        print(CreateBox(1,1,1, false));
        print(CreateBox(1,1,1, true));
        SetPosition(0,0,0, 1);

        objs.Add(Circle);
        objs.Add(BoxOne);
        objs.Add(BoxTwo);
    }

    // Update is called once per frame
    void Update()
    {
        List<GameObject> testedObjectsPASS = new List<GameObject>();

        SetPosition(Circle.transform.position.x, Circle.transform.position.y, Circle.transform.position.z, 0);
        SetPosition(BoxOne.transform.position.x, BoxOne.transform.position.y, BoxOne.transform.position.z, 1);
        SetPosition(BoxTwo.transform.position.x, BoxTwo.transform.position.y, BoxTwo.transform.position.z, 2);

        ObjectBoxHull3D two = BoxTwo.GetComponent<ObjectBoxHull3D>();

        for (int i = 0; i < two.verticies.Length; i++)
        {
            SetPositionWithRotation(two.verticies[i].x, two.verticies[i].y, two.verticies[i].z, i, 2);

        }
        SetPositionWithRotation(two.rightAxis.x, two.rightAxis.y, two.rightAxis.z, 8, 2);
        SetPositionWithRotation(two.upAxis.x, two.upAxis.y, two.upAxis.z, 9, 2);
        SetPositionWithRotation(two.forwardAxis.x, two.forwardAxis.y, two.forwardAxis.z, 10, 2);

        Vector3 obbPostion = BoxTwo.transform.InverseTransformPoint(Circle.transform.position);
        SetPositionOBBSphere(obbPostion.x, obbPostion.y, obbPostion.z, 0);

        for (int i = 0; i < 3; i++)
        {
            for (int j = i + 1; j < 3; j++)
            {
                if (TestCollision(i, j))
                {
                    print("COLLISION");
                    testedObjectsPASS.Add(objs[i]);
                    testedObjectsPASS.Add(objs[j]);
                   
                }
                else
                {
                    objs[i].GetComponent<MeshRenderer>().material = NormalMat;
                    objs[j].GetComponent<MeshRenderer>().material = NormalMat;
                }
            }
        }

        foreach (GameObject obj in testedObjectsPASS)
        {
            obj.GetComponent<MeshRenderer>().material = collisionMat;
        }

    }

    private void OnApplicationQuit()
    {
        Debug.Break();
        CleanPlugin();
    }
}
