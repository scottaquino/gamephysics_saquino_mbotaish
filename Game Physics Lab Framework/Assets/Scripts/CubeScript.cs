﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeScript : MonoBehaviour
{

    private float _heldTimer = 0;
    private float _heldTimeDelay = 0.05f;
    MeshRenderer objMesh;
    Particle3D _particle3D;

    // Start is called before the first frame update
    void Start()
    {
        objMesh = gameObject.GetComponent<MeshRenderer>();
        _particle3D = GetComponent<Particle3D>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnMouseDown()
    {
        CubeManagerScript.instance.SetActiveCube(_particle3D);
    }

    private void OnMouseDrag()
    {
        _heldTimer += Time.deltaTime;
        if (_heldTimer > _heldTimeDelay)
        {
            Vector3 mouse = Input.mousePosition;
            mouse.z = 10;
            transform.position = Camera.main.ScreenToWorldPoint(mouse);
        }

    }

    private void OnMouseUp()
    {
        _heldTimer = 0;
    }

    public void SetFreeFall() { _particle3D.SetFreeFall(); }
    public void SetSideways() { _particle3D.SetSideways(); }
    public void SetCombined() { _particle3D.SetCombined(); }
    public void SetSpring() { _particle3D.SetSpring(); }
    public void SetResistanceFriction() { _particle3D.SetResistanceFriction(); }
    public void SetResistanceDrag() { _particle3D.SetResistanceDrag(); }
    public void SetResistanceCombined() { _particle3D.SetResistanceCombined(); }
    public void SetResistanceNone() { _particle3D.SetResistanceNone(); }
    public void SetForceTypeToImpulse() { _particle3D.SetForceTypeToImpulse(); }


}
