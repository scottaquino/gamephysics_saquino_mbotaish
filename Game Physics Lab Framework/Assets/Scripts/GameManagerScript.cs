﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManagerScript : MonoBehaviour
{
    public static GameManagerScript instance;

    public float minSpawnDelay = 0.5f;
    public float maxSpawnDelay = 5f;

    public float spawnRangeMin = 11;
    public float spawnRangeMax = 18;

    public int score = 0;
    public int time = 60;

    GameObject ps;
    public Material collisionMat;

    private bool hasActiveGrenade = false;

    GameObject asteroidPrefab = null;
    [SerializeField] GameObject scoreUI = null;
    [SerializeField] GameObject timerUI = null;
    [SerializeField] AudioClip bop = null;

    DataManagerScript dmScript;
    AudioSource audio;
    private List<GameObject> activeAsteriods = new List<GameObject>();

    public bool IsActiveGrenadeLive() { return hasActiveGrenade; }

    private void Start()
    {
        instance = this;
        Instantiate();
    }

    private void Instantiate()
    {
        ps = Resources.Load("NEW CONFETTI") as GameObject;
        asteroidPrefab = Resources.Load("Asteroid") as GameObject;
        audio = gameObject.GetComponent<AudioSource>();
        StartCoroutine(SpawnAsteroid());
        StartCoroutine(Timer());
        timerUI.GetComponent<Text>().text = "Time: " + time.ToString();
        dmScript = GameObject.Find("DataManager").GetComponent<DataManagerScript>();
    }

    // Update is called once per frame
    private void Update()
    {
        if (time <= 0)
        {
            dmScript.score = score;
            SceneManager.LoadScene("EndScene");
        }
    }

    IEnumerator Timer()
    {
        yield return new WaitForSeconds(1);
        --time;
        timerUI.GetComponent<Text>().text = "Time: " + time.ToString();
        StartCoroutine(Timer());
    }

    IEnumerator SpawnAsteroid()
    {
        int tempY = (Random.Range(0, 2) * 2) - 1;
        tempY /= Mathf.Abs(tempY);

        float xPos = Random.Range(-spawnRangeMax, spawnRangeMax);
        float yPos = Random.Range(spawnRangeMin, spawnRangeMax) * tempY;

        Vector3 spawnPos = new Vector3(xPos, yPos, 0);
        GameObject tempObject = Instantiate(asteroidPrefab, spawnPos, Quaternion.identity);
        CollisionManager.instance.AddObject(tempObject.GetComponent<CircleCollisionHull2D>());

        activeAsteriods.Add(tempObject);
        
        yield return new WaitForSeconds(Random.Range(minSpawnDelay, maxSpawnDelay));
        StartCoroutine(SpawnAsteroid());
    }

    public void SetScore(int newScore)
    {
        score = newScore;
        scoreUI.GetComponent<Text>().text = "Score: " + newScore.ToString();
    }

    public void RemoveFromList(GameObject obj)
    {
        activeAsteriods.Remove(obj);
    }

    public IEnumerator GRENADE_GET_DOWN(GameObject obj)
    {
        hasActiveGrenade = true;
        while(obj != null)
        {
            for (int i = 0; i < activeAsteriods.Count; i++)
            {
                Vector3 direction = obj.transform.position - activeAsteriods[i].transform.position;
                activeAsteriods[i].GetComponent<Particle2D>().AddForce(direction.normalized * 5.0f);
            }
            yield return null;
        }
    }

    public void Explode(GameObject obj, int radius)
    {
        List<GameObject> objsToDestroy = new List<GameObject>();
        for (int i = 0; i < activeAsteriods.Count; i++)
        {
            if(Vector3.Distance(activeAsteriods[i].transform.position, obj.transform.position) <= radius / 2.0f)
            {             

                if (activeAsteriods[i].GetComponent<AsteroidScript>().canDestroy)
                {
                    GameObject tempPS = Instantiate(ps, activeAsteriods[i].transform.position, Quaternion.identity);

                    Destroy(tempPS, 20);
                    objsToDestroy.Add(activeAsteriods[i]);
                    SetScore(++score);
                }
                activeAsteriods[i].GetComponent<AsteroidScript>().canDestroy = true;
                activeAsteriods[i].GetComponent<MeshRenderer>().material = collisionMat;
            }
        }

        for (int i = 0; i < objsToDestroy.Count; i++)
        {
            activeAsteriods.Remove(objsToDestroy[i]);
            Destroy(objsToDestroy[i]);
        }
        objsToDestroy.Clear();
        hasActiveGrenade = false;

        if(audio)
            audio.PlayOneShot(bop);
    }
}
