﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CubeManagerScript : MonoBehaviour
{

    public static CubeManagerScript instance;

    public InputField amp;
    public InputField freq;
    public Dropdown forceDrop;
    public Dropdown forceType;
    public Dropdown resistDrop;
    public Dropdown inertiaType;
    private Particle3D _activeCube = null;
    public Text _currentState;
    public Slider _slider;

    public List<GameObject> infoPanel = new List<GameObject>();

    public Text[] axiis = new Text[3];

    private float torque = 0;
    private Vector3 axis = Vector3.zero;

    private void Awake()
    {
        instance = this;
    }

    private void FixedUpdate()
    {
        if(_activeCube)
            _activeCube.GetComponent<Particle3D>().GetTorqueInput(); //Add torque based on input (A left, D right)
    }

    public void ChangePanel(Int32 activeNum)
    {
        for (int i  = 0; i < infoPanel.Count; i++)
            infoPanel[i].SetActive(i == (int)activeNum);
        
    }

    public void SetActiveCube(Particle3D particle)
    {
        if (_activeCube)
            _activeCube.GetComponent<MeshRenderer>().material.color = Color.white;

        _activeCube = particle;
        _activeCube.GetComponent<MeshRenderer>().material.color = Color.red;

        amp.text = _activeCube.GetAmp().ToString();
        freq.text = _activeCube.GetFrequency().ToString();

        _currentState.text = "State: " + (particle.GetSetting());
        //_slider.value = particle.GetTorque();
    }

    public void SetAmp()
    {
        if(_activeCube)
        {
            _activeCube.SetAmp(Int32.Parse(amp.text));
            switch (_activeCube.GetSetting())
            {
                case "EULER":
                    SetEuler();
                    break;
                case "KINEMATIC":
                    SetKinematic();
                    break;
            }
        }
    }

    public void SetFrequency()
    {
        if (_activeCube)
        {
            _activeCube.SetFrequency(Int32.Parse(freq.text));
            switch (_activeCube.GetSetting())
            {
                case "EULER":
                    SetEuler();
                    break;
                case "KINEMATIC":
                    SetKinematic();
                    break;
            }
        }
    }


    public void SetEuler()
    {
        if (_activeCube)
        {
            _activeCube.SetEuler();
            _currentState.text = "State: " + (_activeCube.GetSetting());           
        }         
    }

    public void SetKinematic()
    {
        if (_activeCube)
        {
            _activeCube.SetKinematic();
            _currentState.text = "State: " + (_activeCube.GetSetting());            
        }
    }

    public void SetEulerZ()
    {
        if (_activeCube)
        {
            _activeCube.SetEulerZ();
            _currentState.text = "State: " + (_activeCube.GetSetting());
        }
    }

    public void ResetCube()
    {
        if (_activeCube)
            _activeCube.ResetCube(Vector3.zero);
    }

    public void SetForceOption(GameObject cube)
    {
        switch (forceDrop.value)
        {
            case 0:
                SetFreeFall(cube);
                break;
            case 1:
                SetSideways(cube);
                break;
            case 2:
                SetCombined(cube);
                break;
            case 3:
                SetSpring(cube);
                break;
        }
    }

    public void SetResistanceOption(GameObject cube)
    {
        switch (resistDrop.value)
        {
            case 0:
                SetResistanceNone(cube);
                break;
            case 1:
                SetResistanceFriction(cube);
                break;
            case 2:
                SetResistanceDrag(cube);
                break;
            case 3:
                SetResistanceCombined(cube);
                break;
        }
    }

    public void SetMomentOfInertiaType()
    {
        if (_activeCube)
        {
            _activeCube.SetInertiaType(inertiaType.value);
        }
    }

    public void SetForceTypeOption(GameObject cube)
    {
        if(forceType.value == 1)
        {
            SetForceTypeToImpulse(cube);
        }
        
    }

    void SetFreeFall(GameObject cube)
    {
        cube.GetComponent<Particle3D>().SetFreeFall();
    }

    void SetSideways(GameObject cube)
    {
        cube.GetComponent<Particle3D>().SetSideways();
    }

    void SetCombined(GameObject cube)
    {
        cube.GetComponent<Particle3D>().SetCombined();
    }

    void SetSpring(GameObject cube)
    {
        cube.GetComponent<Particle3D>().SetSpring();
    }

    void SetResistanceCombined(GameObject cube)
    {
        cube.GetComponent<Particle3D>().SetResistanceCombined();
    }

    void SetResistanceFriction(GameObject cube)
    {
        cube.GetComponent<Particle3D>().SetResistanceFriction();
    }

    void SetResistanceDrag(GameObject cube)
    {
        cube.GetComponent<Particle3D>().SetResistanceDrag();
    }

    void SetResistanceNone(GameObject cube)
    {
        cube.GetComponent<Particle3D>().SetResistanceNone();
    }

    void SetForceTypeToImpulse(GameObject cube)
    {
        cube.GetComponent<Particle3D>().SetForceTypeToImpulse();
       // cube.GetComponent<Particle3D>().InitialPush();
    }

    public void SetTorqueForce(Vector3 torque) {
        if(_activeCube)
            _activeCube.SetTorqueForce(torque);
    }

    public void SetYAxis(float newAxis)
    {
        axis.y = newAxis;
        axiis[1].text = "Y - " + newAxis;
        if (!_activeCube)
            return;
        axis.y = newAxis;
       _activeCube.SetRotationAxis(axis);
    }

    public void SetXAxis(float newAxis)
    {
        axis.x = newAxis;
        axiis[0].text = "X - " + newAxis;
        if (!_activeCube)
            return;
        axis.x = newAxis;
        _activeCube.SetRotationAxis(axis);
    }


    public void SetZAxis(float newAxis)
    {
        axis.z = newAxis;
        axiis[2].text = "Z - " + newAxis;
        if (!_activeCube)
            return;       
        _activeCube.SetRotationAxis(axis);
    }


    public void SetSettings(GameObject cube)
    {
        Particle3D particle = cube.GetComponent<Particle3D>();
        particle.SetRotationAxis(axis);

    }
}
