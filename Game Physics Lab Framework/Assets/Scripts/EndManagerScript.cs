﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class EndManagerScript : MonoBehaviour
{
    [SerializeField]
    GameObject scoreUI;

    GameObject dataManager;

    // Start is called before the first frame update
    void Start()
    {
        dataManager = GameObject.Find("DataManager");

        if (dataManager)
        {
            scoreUI.GetComponent<Text>().text = dataManager.GetComponent<DataManagerScript>().score.ToString();
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void RestartGame()
    {
        SceneManager.LoadScene("StartScene");
    }
}
