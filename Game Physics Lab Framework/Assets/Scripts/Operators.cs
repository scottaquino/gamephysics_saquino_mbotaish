﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Operators
{
    public static Quaternion Multiply (Quaternion lhs, Vector3 rhs)
    {
        //Multiply quaternion with vector as a quaternion
        //Quaternion vectAsQuat = new Quaternion(rhs.x, rhs.y, rhs.z, 0);
        //Quaternion temp = lhs * vectAsQuat;
        //return temp;

        Vector3 v0, v1;
        float w0;
        w0 = lhs.w;
        v0 = new Vector3(lhs.x, lhs.y, lhs.z);
        v1 = rhs;

        float vDot = Vector3.Dot(v0, v1);
        float tempW = -vDot;
        Vector3 tempVect = w0 * v1 + Vector3.Cross(v0, v1);

        return new Quaternion(tempVect.x, tempVect.y, tempVect.z, tempW);
    }

    public static Quaternion Multiply (Quaternion lhs, params float[] rhs)
    {
        Quaternion temp = lhs;

        //Multiply each part by each scalar
        for (int i  = 0; i < rhs.Length; ++i)
        {
            temp = new Quaternion(temp.x * rhs[i], temp.y * rhs[i], temp.z * rhs[i], temp.w * rhs[i]);
        }
      
        return temp;
    }

    public static Quaternion Add (Quaternion lhs, Quaternion rhs)
    {
        //Add all the parts together
        return new Quaternion(lhs.x + rhs.x, lhs.y + rhs.y, lhs.z + rhs.z, lhs.w + rhs.w);
    }
}