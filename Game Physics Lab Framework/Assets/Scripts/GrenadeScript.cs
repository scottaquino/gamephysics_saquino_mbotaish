﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrenadeScript : MonoBehaviour
{

    public int radius;

    float width, height;
    float timer = 0;
    float delay = 0.75f;
    Particle2D particle;
    GameObject circle;
    

    public void Start()
    {
        width = Screen.width;
        height = Screen.height;
        particle = GetComponent<Particle2D>();
        circle = transform.GetChild(0).gameObject;
        transform.GetChild(1).localScale = new Vector3(1, 1, 1) * radius * 2.0f;
    }

    private void Update()
    {
        UpdateCircle();
        CheckBounds();
    }

    private void UpdateCircle()
    {
        circle.transform.localScale = Vector3.Lerp(new Vector3(1, 1, 1) * radius * 2.0f, new Vector3(1, 1, 1), timer / delay);
        timer = (timer + Time.deltaTime) % delay;
    }

    private void CheckBounds()
    {
        Vector3 pos = Camera.main.WorldToScreenPoint(gameObject.transform.position);
        if (pos.x < 0)
        {
            particle.position.x = Camera.main.ScreenToWorldPoint(new Vector3(0, pos.y)).x;
            Vector2 vel = particle.velocity;
            vel.x *= -1;
            particle.velocity = (vel);
        }
        if (pos.x >= width)
        {
            particle.position.x = Camera.main.ScreenToWorldPoint(new Vector3(width, pos.y)).x;
            Vector2 vel = particle.velocity;
            vel.x *= -1;
            particle.velocity = (vel);
        }

        if (pos.y < 0)
        {
            particle.position.y = Camera.main.ScreenToWorldPoint(new Vector3(pos.x, 0)).y;
            Vector2 vel = particle.velocity;
            vel.y *= -1;
            particle.velocity = (vel);
        }
        if (pos.y >= height)
        {
            particle.position.y = Camera.main.ScreenToWorldPoint(new Vector3(pos.x, height)).y;
            Vector2 vel = particle.velocity;
            vel.y *= -1;
            particle.velocity = (vel);
        }
    }

    private void OnDestroy()
    {
        GameManagerScript.instance.Explode(gameObject, radius);
    }
}
