﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AsteroidScript : MonoBehaviour
{
    public float randomX;
    public float randomY;

    public float minSpeed = 2;
    public float maxSpeed = 10;

    public bool canDestroy = false;

    // Start is called before the first frame update
    void Start()
    {
        Vector3 target = new Vector3(Random.Range(-randomX, randomX), Random.Range(-randomY, randomY), 0f);
        target = target - transform.position;
        float scale = Random.Range(0.25f, 1);
        transform.localScale = new Vector3(scale, scale, scale);
        GetComponent<CircleCollisionHull2D>().radius = scale * 0.5f;
        GetComponent<Particle2D>().AddForce(target.normalized * Random.Range(minSpeed, maxSpeed));
    }

    private void OnDestroy()
    {
        GameManagerScript.instance.RemoveFromList(gameObject);
    }
}
