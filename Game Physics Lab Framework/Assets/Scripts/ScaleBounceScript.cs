﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScaleBounceScript : MonoBehaviour
{
    public float speed;
    public float distance;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.localScale = new Vector3(1f + Mathf.PingPong(Time.time * speed, distance), 1f + Mathf.PingPong(Time.time * speed, distance), transform.localScale.z);
    }
}
