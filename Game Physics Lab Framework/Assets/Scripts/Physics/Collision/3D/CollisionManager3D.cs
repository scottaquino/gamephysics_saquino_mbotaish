﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionManager3D : MonoBehaviour
{

    public static CollisionManager3D instance;

    private List<CollisionHull3D> _objectsToCheck = new List<CollisionHull3D>();

    public Material collisionMat;
    public Material startMat;

    private void Awake()
    {
        instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {      
        _objectsToCheck = new List<CollisionHull3D>(FindObjectsOfType<CollisionHull3D>());
    }

    public void FixedUpdate()
    {
        CheckObjects();
    }

    private void CheckObjects()
    {
        CollisionHull3D.Collision contact = new CollisionHull3D.Collision();
        List<CollisionHull3D> testedObjectsPASS = new List<CollisionHull3D>();

        for (int i = 0; i < _objectsToCheck.Count; i++)
        {
            for (int j = i + 1; j < _objectsToCheck.Count; j++)
            {
                if (CollisionHull3D.TestCollision(_objectsToCheck[i], _objectsToCheck[j], ref contact)) // REPLACE THIS CHECK WITH ONE IN DLL
                {
                    testedObjectsPASS.Add(_objectsToCheck[i]);
                    testedObjectsPASS.Add(_objectsToCheck[j]);
                }
                else
                {
                   //_objectsToCheck[i].gameObject.GetComponent<MeshRenderer>().material = startMat;
                   //_objectsToCheck[j].gameObject.GetComponent<MeshRenderer>().material = startMat;
                }
            }
        }

        foreach (CollisionHull3D obj in testedObjectsPASS)
        {
            //obj.gameObject.GetComponent<MeshRenderer>().material = collisionMat;
        }

        testedObjectsPASS.Clear();
    }
}
