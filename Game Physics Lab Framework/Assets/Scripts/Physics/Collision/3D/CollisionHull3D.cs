﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class CollisionHull3D : MonoBehaviour
{

    public class Collision
    {
        public struct Contact
        {
            public Vector3 point;
            public Vector3 normal;
        }

        public float restitutionA, restitutionB;
        public CollisionHull3D a = null, b = null;
        public Contact[] contact = new Contact[4];
        public int contactCount = 0;
        public bool status = false;

        public float separatingVelocity;
        public float penetration;
    }

    public float restitution = 1;

    public enum CollisionHullType3D
    {
        SPHERE,
        AABB,
        OBB,
    }

    protected Particle3D particle;

    public Particle3D GetParticle() { return particle; }
    public CollisionHullType3D type { get; }
    protected CollisionHull3D(CollisionHullType3D type_set) { type = type_set; }

    virtual protected void Start() { particle = GetComponent<Particle3D>(); }

    public static bool TestCollision(CollisionHull3D a, CollisionHull3D b, ref Collision c)
    {
        if (b.type == CollisionHullType3D.SPHERE) { return a.TestCollision(b as SphereHull3D, ref c); }
        else if (b.type == CollisionHullType3D.AABB) { return a.TestCollision(b as AxisAlignHull3D, ref c); }
        else if (b.type == CollisionHullType3D.OBB) { return a.TestCollision(b as ObjectBoxHull3D, ref c); }
        else { Debug.LogError("Object not set to correct collision type"); }

        c.status = false;
        return false;
    }

    public static bool Vector3Comparison(Vector3 left, Vector3 right)
    {
        if (left.x >= right.x && left.y >= right.y && left.z >= right.z)
        {
            return true;
        }
        return false;
    }

    public abstract bool TestCollision(SphereHull3D other, ref Collision c);
    public abstract bool TestCollision(AxisAlignHull3D other, ref Collision c);
    public abstract bool TestCollision(ObjectBoxHull3D other, ref Collision c);

}
