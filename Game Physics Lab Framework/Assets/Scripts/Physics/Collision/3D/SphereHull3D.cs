﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SphereHull3D : CollisionHull3D
{
    public SphereHull3D() : base(CollisionHullType3D.SPHERE) { }

    [Range(0.0f, 100.0f)]
    public float radius;

    public override bool TestCollision(SphereHull3D other, ref Collision c) {

        Vector3 distance = other.gameObject.transform.position - gameObject.transform.position;
        float radiiSum = other.radius + radius;

        if(Vector3.Dot(distance, distance) <= radiiSum * radiiSum)
        {
            c.a = this;
            c.b = other;

            c.status = true;
            Vector3 normDist = distance.normalized;

            c.contact[0].point = normDist * radius + gameObject.transform.position;
            c.contact[0].normal = normDist;
            c.restitutionA = restitution;
            c.restitutionB = other.restitution;
            c.contactCount = 1;
            c.separatingVelocity = Vector3.Dot((particle.velocity - other.particle.velocity), c.contact[0].normal);
            c.penetration = radiiSum - (other.transform.position - transform.position).magnitude;

            return true;
        }

        return false;
    }

    public override bool TestCollision(AxisAlignHull3D other, ref Collision c) {
        // calculate closest point by clamping circle center on each dimension
        // passes if closest point vs circle passes
        // 1. Get circle position
        // 2. Get AABB object position
        // 3. Clamp to nearest point on object
        // 4. Find difference of circlePos - objPos on each axis
        // 5. Compare distance of point to length of radius
        // 6. return results

      
        Vector3 relCenter = other.transform.InverseTransformPoint(transform.position);

        Vector3 closestPoint = Vector3.zero;
        float dist = relCenter.x;
        if (dist > other.half_dimensions.x) dist = other.half_dimensions.x;
        if (dist < -other.half_dimensions.x) dist = -other.half_dimensions.x;
        closestPoint.x = dist;

        dist = relCenter.y;
        if (dist > other.half_dimensions.y) dist = other.half_dimensions.y;
        if (dist < -other.half_dimensions.y) dist = -other.half_dimensions.y;
        closestPoint.y = dist;

        dist = relCenter.z;
        if (dist > other.half_dimensions.z) dist = other.half_dimensions.z;
        if (dist < -other.half_dimensions.z) dist = -other.half_dimensions.z;
        closestPoint.z = dist;


        dist = (closestPoint - relCenter).sqrMagnitude;

      

        if (!(dist > radius * radius))
        {
            /*
            *  1) Get the closest contact point
            *  2) Get the normal of collision 
            *  3) set the restitution of each object 
            *  4) Set the contact count 
            *  5) Set the separating velocity 
            *  6) Get the penetration depth 
           */

            /*
            c.a = this;
            c.b = other;

            float clampedX = Mathf.Clamp(transform.position.x, other.bottomLeft.x, other.bottomRight.x);
            float clampedY = Mathf.Clamp(transform.position.y, other.bottomLeft.y, other.topLeft.y);

            Vector2 contactPoint = new Vector2(clampedX, clampedY);
            Vector2 surfaceNormal = ((Vector2)transform.position - contactPoint).normalized;

            c.contact[0].point = contactPoint;
            c.contact[0].normal = new Vector2(Mathf.Round(surfaceNormal.x), Mathf.Round(surfaceNormal.y));
            c.restitutionA = restitution;
            c.contactCount = 1;
            c.restitutionB = other.restitution;
            c.separatingVelocity = Vector3.Dot(((Vector3)other.GetParticle().velocity - (Vector3)particle.velocity), c.contact[0].normal);

            c.penetration = radius - ((Vector2)transform.position - contactPoint).magnitude;
            */

            return true;            
        }

        return false;
    }

    public override bool TestCollision(ObjectBoxHull3D other, ref Collision c) {
        // calculate closest point by clamping circle center on each dimension
        // passes if closest point vs circle passes
        // 1. Get circle position
        // 2. Get AABB object position
        // 3. Clamp to nearest point on object
        // 4. Find difference of circlePos - objPos on each axis
        // 5. Compare distance of point to length of radius
        // 6. return results

        Vector3 relCenter = other.transform.InverseTransformPoint(transform.position);

        Vector3 closestPoint = Vector3.zero;
        float dist = relCenter.x;
        if (dist > other.half_dimensions.x) dist = other.half_dimensions.x;
        if (dist < -other.half_dimensions.x) dist = -other.half_dimensions.x;
        closestPoint.x = dist;

        dist = relCenter.y;
        if (dist > other.half_dimensions.y) dist = other.half_dimensions.y;
        if (dist < -other.half_dimensions.y) dist = -other.half_dimensions.y;
        closestPoint.y = dist;

        dist = relCenter.z;
        if (dist > other.half_dimensions.z) dist = other.half_dimensions.z;
        if (dist < -other.half_dimensions.z) dist = -other.half_dimensions.z;
        closestPoint.z = dist;


        dist = (closestPoint - relCenter).sqrMagnitude;

        if (!(dist > radius * radius))
        {
            /*
            *  1) Get the closest contact point
            *  2) Get the normal of collision 
            *  3) set the restitution of each object 
            *  4) Set the contact count 
            *  5) Set the separating velocity 
            *  6) Get the penetration depth 
           */

            /*
            c.a = this;
            c.b = other;

            float clampedX = Mathf.Clamp(transform.position.x, other.bottomLeft.x, other.bottomRight.x);
            float clampedY = Mathf.Clamp(transform.position.y, other.bottomLeft.y, other.topLeft.y);

            Vector2 contactPoint = new Vector2(clampedX, clampedY);
            Vector2 surfaceNormal = ((Vector2)transform.position - contactPoint).normalized;

            c.contact[0].point = contactPoint;
            c.contact[0].normal = new Vector2(Mathf.Round(surfaceNormal.x), Mathf.Round(surfaceNormal.y));
            c.restitutionA = restitution;
            c.contactCount = 1;
            c.restitutionB = other.restitution;
            c.separatingVelocity = Vector3.Dot(((Vector3)other.GetParticle().velocity - (Vector3)particle.velocity), c.contact[0].normal);

            c.penetration = radius - ((Vector2)transform.position - contactPoint).magnitude;
            */

            return true;
        }

        return false;
    }
}
                    

