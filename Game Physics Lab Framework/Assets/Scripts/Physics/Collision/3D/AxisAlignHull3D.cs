﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AxisAlignHull3D : CollisionHull3D
{
    public AxisAlignHull3D() : base(CollisionHullType3D.AABB) { }

    public Vector3 dimensions;

    public Vector3 FtopLeft;
    public Vector3 FtopRight;
    public Vector3 FbottomLeft;
    public Vector3 FbottomRight;
    public Vector3 BtopLeft;
    public Vector3 BtopRight;
    public Vector3 BbottomLeft;
    public Vector3 BbottomRight;

    public Vector3 half_dimensions;

    Vector3[] axis = new Vector3[15];
    Vector3[] verticies = new Vector3[8];

    public Matrix4x4 transMaxtrix;
    // Start is called before the first frame update
    protected override void Start()
    {
        base.Start();
        half_dimensions = dimensions * 0.5f;

        UpdateBounds();
    }

    // Update is called once per frame
    void Update()
    {
        UpdateBounds();
    }

    public void UpdateBounds()
    {
        BbottomLeft = -half_dimensions;
        FtopRight = half_dimensions;

        FtopLeft = new Vector3(BbottomLeft.x, FtopRight.y, FtopRight.z);
        FbottomLeft = new Vector3(BbottomLeft.x, -FtopRight.y, FtopRight.z);
        FbottomRight = new Vector3(FtopRight.x, BbottomLeft.y, FtopRight.z);

        BbottomRight = new Vector3(FtopRight.x, BbottomLeft.y, BbottomLeft.z);
        BtopLeft = new Vector3(BbottomLeft.x, FtopRight.y, BbottomLeft.z);

        BtopRight = new Vector3(FtopRight.x, -BbottomLeft.y, BbottomLeft.z);
        transMaxtrix = transform.localToWorldMatrix;

        BbottomLeft = transMaxtrix.MultiplyPoint(BbottomLeft);
        BbottomRight = transMaxtrix.MultiplyPoint(BbottomRight);
        BtopLeft = transMaxtrix.MultiplyPoint(BtopLeft);
        BtopRight = transMaxtrix.MultiplyPoint(BtopRight);

        FtopRight = transMaxtrix.MultiplyPoint(FtopRight);
        FtopLeft = transMaxtrix.MultiplyPoint(FtopLeft);
        FbottomLeft = transMaxtrix.MultiplyPoint(FbottomLeft);
        FbottomRight = transMaxtrix.MultiplyPoint(FbottomRight);

        verticies[0] = FtopLeft;    
        verticies[1] = FtopRight;
        verticies[2] = FbottomLeft;
        verticies[3] = FbottomRight;

        verticies[4] = BtopLeft;
        verticies[5] = BtopRight;
        verticies[6] = BbottomLeft;
        verticies[7] = BbottomRight;
    }


    public override bool TestCollision(SphereHull3D other, ref Collision c) {
        return other.TestCollision(this, ref c);
    }

    public override bool TestCollision(AxisAlignHull3D other, ref Collision c) {

        // for each dimension, max extent of A is greater than min extent of B
        //                     min extent of A is less than max extent of B
        //1. Get the demensions of each object
        //2. Calculate the min and max extent in each dimension 
        //3. Test dimension 
        //4. return results;

        Vector3 dimensoinsOther = other.dimensions * 0.5f;

        float maxX = transform.position.x + half_dimensions.x;
        float minX = transform.position.x - half_dimensions.x;
        float maxY = transform.position.y + half_dimensions.y;
        float minY = transform.position.y - half_dimensions.y;
        float maxZ = transform.position.z + half_dimensions.z;
        float minZ = transform.position.z - half_dimensions.z;

        float minXOther = other.gameObject.transform.position.x - dimensoinsOther.x;
        float maxXOther = other.gameObject.transform.position.x + dimensoinsOther.x;
        float minYOther = other.gameObject.transform.position.y - dimensoinsOther.y;
        float maxYOther = other.gameObject.transform.position.y + dimensoinsOther.y;
        float minZOther = other.gameObject.transform.position.z - dimensoinsOther.z;
        float maxZOther = other.gameObject.transform.position.z + dimensoinsOther.z;



        if ((maxX >= minXOther && minX <= maxXOther) &&
            (maxY >= minYOther && minY <= maxYOther) &&
            (maxZ >= minZOther && minZ <= maxZOther))
        {

            /*
              *  1) Get the closest contact point
              *  2) Get the normal of collision 
              *  3) set the restitution of each object 
              *  4) Set the contact count 
              *  5) Set the separating velocity 
              *  6) Get the penetration depth 
             */

            c.a = this;
            c.b = other;

            float clampedX = Mathf.Clamp(transform.position.x, minXOther, maxXOther);
            float clampedY = Mathf.Clamp(transform.position.y, minYOther, maxYOther);

            Vector2 contactPoint = new Vector2(clampedX, clampedY);
            Vector2 surfaceNormal = ((Vector2)transform.position - contactPoint).normalized;

            c.contact[0].point = contactPoint;
            c.contact[0].normal = new Vector2(Mathf.Round(surfaceNormal.x), Mathf.Round(surfaceNormal.y));
            c.restitutionA = restitution;
            c.contactCount = 1; //Maybe more?
            c.restitutionB = other.restitution;
            c.separatingVelocity = Vector3.Dot(((Vector3)particle.velocity - (Vector3)other.GetParticle().velocity), c.contact[0].normal);

            return true;
        }


        return false;
    }
    public override bool TestCollision(ObjectBoxHull3D other, ref Collision c)
    {
        // 1. Find all the axiis on one of the OBB object
        // 2. Project all points onto each axis for both OBB objects
        // 3. Find the min and max points of each OBB object
        // 3. Compare the min and maxs of each point
        // 4. If there is not over lap return false (no collision)
        // 5. Repeat steps 1-4 with the other OBB object axiis 
        // 6. If no test fails return true


        axis[0] = Vector3.right;
        axis[1] = Vector3.up;
        axis[2] = Vector3.forward;
        axis[3] = other.rightAxis;
        axis[4] = other.upAxis;
        axis[5] = other.forwardAxis;
        axis[6] = Vector3.Cross(Vector3.right, other.rightAxis);
        axis[7] = Vector3.Cross(Vector3.right, other.upAxis);
        axis[8] = Vector3.Cross(Vector3.right, other.forwardAxis);
        axis[9] = Vector3.Cross(Vector3.up, other.rightAxis);
        axis[10] = Vector3.Cross(Vector3.up, other.upAxis);
        axis[11] = Vector3.Cross(Vector3.up, other.forwardAxis);
        axis[12] = Vector3.Cross(Vector3.forward, other.rightAxis);
        axis[13] = Vector3.Cross(Vector3.forward, other.upAxis);
        axis[14] = Vector3.Cross(Vector3.forward, other.forwardAxis);

        for (int x = 0; x < axis.Length; ++x)
        {
            if (axis[x] == Vector3.zero)
            {
                continue;
            }

            float minOBB1 = float.MaxValue;
            float minOBB2 = float.MaxValue;
            float maxOBB1 = float.MinValue;
            float maxOBB2 = float.MinValue;
            string s = x + ") ";

            for (int i = 0; i < 8; ++i)
            {
                float obbDistance1 = Vector3.Dot(verticies[i], axis[x]);
               
               

                if (obbDistance1 < minOBB1) { minOBB1 = obbDistance1; }
                if (obbDistance1 > maxOBB1) { maxOBB1 = obbDistance1; }

                float obbDistance2 = Vector3.Dot(other.verticies[i], axis[x]);
                s += i + ") " + obbDistance2 + " \n";
                if (obbDistance2 < minOBB2) { minOBB2 = obbDistance2; }
                if (obbDistance2 > maxOBB2) { maxOBB2 = obbDistance2; }



            }
           
            float distance = Mathf.Max(maxOBB1, maxOBB2) - Mathf.Min(minOBB1, minOBB2);
            print(s);
            float spanDistance = (maxOBB1 - minOBB1) + (maxOBB2 - minOBB2);
            //print((x) + ") " + (maxOBB1) + ", " + (minOBB1) + " | " + (maxOBB2) + ", " + (minOBB2) + '\n');
            if (distance >= spanDistance) { return false; }
        }

        return true;
    }

    string ToString(Vector3 vec)
    {
        return vec.x + ", " + vec.y + "," + vec.z;
    }
}
