﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectBoxHull3D : CollisionHull3D
{
    public ObjectBoxHull3D() : base(CollisionHullType3D.OBB) { }

    public Vector3 dimensions;

    public Vector3 FtopLeft;
    public Vector3 FtopRight;
    public Vector3 FbottomLeft;
    public Vector3 FbottomRight;
    public Vector3 BtopLeft;
    public Vector3 BtopRight;
    public Vector3 BbottomLeft;
    public Vector3 BbottomRight;

    public Vector3[] verticies = new Vector3[8];

    public Vector3 half_dimensions;

    Vector3[] axis = new Vector3[15];

    public Vector3 rightAxis, upAxis, forwardAxis;

    public Matrix4x4 transMaxtrix;
    // Start is called before the first frame update
    protected override void Start()
    {
        base.Start();
        half_dimensions = dimensions * 0.5f;

        UpdateBounds();
    }

    // Update is called once per frame
    void Update()
    {
        UpdateBounds();
    }

    public void UpdateBounds()
    {
        BbottomLeft = -half_dimensions;
        FtopRight = half_dimensions;

        FtopLeft = new Vector3(BbottomLeft.x, FtopRight.y, FtopRight.z);
        FbottomLeft = new Vector3(BbottomLeft.x, -FtopRight.y, FtopRight.z);
        FbottomRight = new Vector3(FtopRight.x, BbottomLeft.y, FtopRight.z);

        BbottomRight = new Vector3(FtopRight.x, BbottomLeft.y, BbottomLeft.z);
        BtopLeft = new Vector3(BbottomLeft.x, FtopRight.y, BbottomLeft.z);

        BtopRight = new Vector3(FtopRight.x, -BbottomLeft.y, BbottomLeft.z);
        transMaxtrix = transform.localToWorldMatrix;

        BbottomLeft = transMaxtrix.MultiplyPoint(BbottomLeft);
        BbottomRight = transMaxtrix.MultiplyPoint(BbottomRight);
        BtopLeft = transMaxtrix.MultiplyPoint(BtopLeft);
        BtopRight = transMaxtrix.MultiplyPoint(BtopRight);

        FtopRight = transMaxtrix.MultiplyPoint(FtopRight);
        FtopLeft = transMaxtrix.MultiplyPoint(FtopLeft);
        FbottomLeft = transMaxtrix.MultiplyPoint(FbottomLeft);
        FbottomRight = transMaxtrix.MultiplyPoint(FbottomRight);

        verticies[0] = BbottomLeft;
        verticies[1] = BbottomRight;
        verticies[2] = BtopLeft;
        verticies[3] = BtopRight;
        verticies[4] = FtopLeft;
        verticies[5] = FtopRight;
        verticies[6] = FbottomLeft;
        verticies[7] = FbottomRight;

        rightAxis = transform.right;
        upAxis = transform.up;
        forwardAxis = transform.forward;
    }

    public override bool TestCollision(SphereHull3D other, ref Collision c) { return other.TestCollision(this, ref c); }
    public override bool TestCollision(AxisAlignHull3D other, ref Collision c) { return other.TestCollision(this, ref c); }

    //https://gamedev.stackexchange.com/questions/44500/how-many-and-which-axes-to-use-for-3d-obb-collision-with-sat
    public override bool TestCollision(ObjectBoxHull3D other, ref Collision c) {
        // 1. Find all the axiis on one of the OBB object
        // 2. Project all points onto each axis for both OBB objects
        // 3. Find the min and max points of each OBB object
        // 3. Compare the min and maxs of each point
        // 4. If there is not over lap return false (no collision)
        // 5. Repeat steps 1-4 with the other OBB object axiis 
        // 6. If no test fails return true

        axis[0] = rightAxis;
        axis[1] = upAxis;
        axis[2] = forwardAxis;
        axis[3] = other.rightAxis;
        axis[4] = other.upAxis;
        axis[5] = other.forwardAxis;
        axis[6] = Vector3.Cross(rightAxis, other.rightAxis);
        axis[7] = Vector3.Cross(rightAxis, other.upAxis);
        axis[8] = Vector3.Cross(rightAxis, other.forwardAxis);
        axis[9] = Vector3.Cross(upAxis, other.rightAxis);
        axis[10] = Vector3.Cross(upAxis, other.upAxis);
        axis[11] = Vector3.Cross(upAxis, other.forwardAxis);
        axis[12] = Vector3.Cross(forwardAxis, other.rightAxis);
        axis[13] = Vector3.Cross(forwardAxis, other.upAxis);
        axis[14] = Vector3.Cross(forwardAxis, other.forwardAxis);

        for (int x = 0; x < axis.Length; ++x)
        {
           if (axis[x] == Vector3.zero)
           {
                continue;
           }

            float minOBB1 = float.MaxValue;
            float minOBB2 = float.MaxValue;
            float maxOBB1 = float.MinValue;
            float maxOBB2 = float.MinValue;

            for (int i = 0; i < 8; ++i)
            {
                float obbDistance1 = Vector3.Dot(verticies[i], axis[x]);
                
                if (obbDistance1 < minOBB1){ minOBB1 = obbDistance1;}
                if (obbDistance1 > maxOBB1){ maxOBB1 = obbDistance1;}

                float obbDistance2 = Vector3.Dot(other.verticies[i], axis[x]);

                if (obbDistance2 < minOBB2){ minOBB2 = obbDistance2;}
                if (obbDistance2 > maxOBB2){ maxOBB2 = obbDistance2;}              

            }

            float distance = Mathf.Max(maxOBB1, maxOBB2) - Mathf.Min(minOBB1, minOBB2);
            float spanDistance = (maxOBB1) - (minOBB1) + (maxOBB2 -minOBB2);
            if (distance >= spanDistance) { return false; }
        }       

        return true;
    }
}
