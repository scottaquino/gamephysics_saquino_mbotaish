﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AxisAlignHull2D : CollisionHull2D
{
    public AxisAlignHull2D() : base(CollisionHullType2D.hull_aabb) { }

    public Vector3 dimensions;

    public Vector3 topLeft;
    public Vector3 topRight;
    public Vector3 bottomLeft;
    public Vector3 bottomRight;
    public Vector3 half_dimensions;
    private LineRenderer line;

    // Start is called before the first frame update
    protected override void Start()
    {
        base.Start();
        line = GetComponent<LineRenderer>();
        half_dimensions = dimensions * 0.5f;

        UpdateBounds();
    }

    // Update is called once per frame
    void Update()
    {
        UpdateBounds();
    }

    private void UpdateBounds()
    {
        bottomLeft = -half_dimensions + transform.position;
        topRight = half_dimensions + transform.position;
        topLeft = new Vector3(bottomLeft.x, topRight.y);
        bottomRight = new Vector3(topRight.x, bottomLeft.y);
        
    }

    public override bool TestCollisionVsCircle(CircleCollisionHull2D other, ref Collision c)
    {
        
        return other.TestCollisionVsAABB(this,ref c);
    }

    public override bool TestCollisionVsAABB(AxisAlignHull2D other, ref Collision c)
    {
        // for each dimension, max extent of A is greater than min extent of B
        //                     min extent of A is less than max extent of B
        //1. Get the demensions of each object
        //2. Calculate the min and max extent in each dimension 
        //3. Test dimension 
        //4. return results;

        Vector3 dimensoinsOther = other.dimensions * 0.5f;

        float maxX = transform.position.x + half_dimensions.x;
        float minX = transform.position.x - half_dimensions.x;
        float maxY = transform.position.y + half_dimensions.y;
        float minY = transform.position.y - half_dimensions.y;

        float minXOther = other.gameObject.transform.position.x - dimensoinsOther.x;
        float maxXOther = other.gameObject.transform.position.x + dimensoinsOther.x;
        float minYOther = other.gameObject.transform.position.y - dimensoinsOther.y;
        float maxYOther = other.gameObject.transform.position.y + dimensoinsOther.y;



        if ((maxX >= minXOther && minX <= maxXOther) &&
            (maxY >= minYOther && minY <= maxYOther))
        {

          /*
            *  1) Get the closest contact point
            *  2) Get the normal of collision 
            *  3) set the restitution of each object 
            *  4) Set the contact count 
            *  5) Set the separating velocity 
            *  6) Get the penetration depth 
           */

            c.a = this;
            c.b = other;

            float clampedX = Mathf.Clamp(transform.position.x, minXOther, maxXOther);
            float clampedY = Mathf.Clamp(transform.position.y, minYOther, maxYOther);

            Vector2 contactPoint = new Vector2(clampedX, clampedY);
            Vector2 surfaceNormal = ((Vector2)transform.position - contactPoint).normalized;

            c.contact[0].point = contactPoint;
            c.contact[0].normal = new Vector2(Mathf.Round(surfaceNormal.x), Mathf.Round(surfaceNormal.y));
            c.restitutionA = restitution;
            c.contactCount = 1; //Maybe more?
            c.restitutionB = other.restitution;
            c.separatingVelocity = Vector3.Dot(((Vector3)particle.velocity - (Vector3)other.GetParticle().velocity), c.contact[0].normal);

            return true;
        }


        return false;
    }

    public override bool TestCollisionVsOBB(ObjectBoxHull2D other, ref Collision c)
    {
        // same as above twice
        // first, test AABB vs max extents of OBB
        // then, multiply by OBB inverse matrix, do test again

        // 1. calculate extents of boxes for AABB and OBB portion
        // 2. multiply by OBB inverse matrix
        // 3. test extents against AABB
        // 4. calculate new OBB using OBB inverse matrix
        // 5. test extents agains new OBB
        // 6. return results              

        Vector2[] OBBAxis = { other.transform.right, other.transform.up, transform.right, transform.up };

        for(int i = 0; i < 4; i++)
        {
            Vector3 otherProjectTL = Vector3.Project(other.topLeft, OBBAxis[i]);
            Vector3 otherProjectTR = Vector3.Project(other.topRight, OBBAxis[i]);
            Vector3 otherProjectBL = Vector3.Project(other.bottomLeft, OBBAxis[i]);
            Vector3 otherProjectBR = Vector3.Project(other.bottomRight, OBBAxis[i]);

            Vector3 ProjectTL = Vector3.Project(topLeft, OBBAxis[i]);
            Vector3 ProjectTR = Vector3.Project(topRight, OBBAxis[i]);
            Vector3 ProjectBL = Vector3.Project(bottomLeft, OBBAxis[i]);
            Vector3 ProjectBR = Vector3.Project(bottomRight, OBBAxis[i]);

            float maxX = transform.position.x + half_dimensions.x;
            float minX = transform.position.x - half_dimensions.x;
            float maxY = transform.position.y + half_dimensions.y;
            float minY = transform.position.y - half_dimensions.y;

            Vector2 maxOBB = new Vector2(Mathf.Max(otherProjectTL.x, otherProjectTR.x, otherProjectBL.x, otherProjectBR.x), Mathf.Max(otherProjectTL.y, otherProjectTR.y, otherProjectBL.y, otherProjectBR.y));
            Vector2 minOBB = new Vector2(Mathf.Min(otherProjectTL.x, otherProjectTR.x, otherProjectBL.x, otherProjectBR.x), Mathf.Min(otherProjectTL.y, otherProjectTR.y, otherProjectBL.y, otherProjectBR.y));

            Vector2 maxABB = new Vector2(Mathf.Max(ProjectTL.x, ProjectTR.x, ProjectBL.x, ProjectBR.x), Mathf.Max(ProjectTL.y, ProjectTR.y, ProjectBL.y, ProjectBR.y));
            Vector2 minABB = new Vector2(Mathf.Min(ProjectTL.x, ProjectTR.x, ProjectBL.x, ProjectBR.x), Mathf.Min(ProjectTL.y, ProjectTR.y, ProjectBL.y, ProjectBR.y));

            if ((!(Vector2Comparison(maxABB, minOBB) && Vector2Comparison(maxOBB, minABB))))
                return false;

           /*
            *  1) Get the closest contact point
            *  2) Get the normal of collision 
            *  3) set the restitution of each object 
            *  4) Set the contact count 
            *  5) Set the separating velocity 
            *  6) Get the penetration depth 
           */


            float clampedX = Mathf.Clamp(other.transform.position.x, minX, maxX);
            float clampedY = Mathf.Clamp(other.transform.position.y, minY, maxY);

            c.a = this;
            c.b = other;

            Vector3 direction = other.transform.position - transform.position;
            Vector3 distanceNorm = direction.normalized;

            c.contact[0].point = new Vector2(clampedX, clampedY);
            c.contact[0].normal = distanceNorm;
            c.restitutionA = restitution;
            c.contactCount = 1; 
            c.restitutionB = other.restitution;
            c.separatingVelocity = Vector3.Dot(((Vector3)particle.velocity - (Vector3)other.GetParticle().velocity), c.contact[0].normal);
        }

        return true;
    }
}
