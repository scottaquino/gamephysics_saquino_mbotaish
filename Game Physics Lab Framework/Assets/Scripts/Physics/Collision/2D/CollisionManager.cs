﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionManager : MonoBehaviour
{
    public static CollisionManager instance;

    private List<CollisionHull2D> _objectsToCheck = new List<CollisionHull2D>();

    public Material collisionMat;
    public Material startMat;
    GameObject ps;

    public float shakeDuration = 0.5f;
    public float shakeFactor = 1;

    public AudioClip shipHitClip;

    private AudioSource audio;
    private GameManagerScript gmScript;
    private bool doShake;
    private Vector3 shake;
    private Vector3 startCameraPos;
    private float shakeReset;

    [SerializeField] AudioClip wow = null;

    private void Awake()
    {
        instance = this;
    }


    // Start is called before the first frame update
    void Start()
    {
        ps = Resources.Load("NEW CONFETTI") as GameObject;
        audio = GetComponent<AudioSource>();
        shakeReset = shakeFactor;
        gmScript = GameObject.Find("GameManager").GetComponent<GameManagerScript>();
        _objectsToCheck = new List<CollisionHull2D>(FindObjectsOfType<CollisionHull2D>());
        startCameraPos = Camera.main.transform.position;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        CheckObjects();

        //Shake camera
        if(doShake)
        {
            shake = new Vector3(Random.Range(-shakeFactor, shakeFactor), Random.Range(-shakeFactor, shakeFactor), Random.Range(-shakeFactor, shakeFactor));
            Camera.main.transform.position += shake;
        }
        else
        {
            shake = Vector3.zero; //There is no shake
        }
    }

    private void CheckObjects()
    {
        CollisionHull2D.Collision contact = new CollisionHull2D.Collision();
        List<CollisionHull2D> testedObjectsPASS = new List<CollisionHull2D>();

        for (int i = 0; i < _objectsToCheck.Count; i++)
        {
            for (int j = i + 1; j < _objectsToCheck.Count; j++)
            {
                if (CollisionHull2D.TestCollision(_objectsToCheck[i], _objectsToCheck[j], ref contact))
                {
                    if (!(_objectsToCheck[i].tag == "Bullet" && _objectsToCheck[j].tag == "Bullet"))
                    {
                        testedObjectsPASS.Add(_objectsToCheck[i]);
                        testedObjectsPASS.Add(_objectsToCheck[j]);
                        SetCollisionVelocity(_objectsToCheck[i], _objectsToCheck[j], contact);
                        CheckCollisionActions(_objectsToCheck[i], _objectsToCheck[j]);
                    }
                }     
            }
        }

        testedObjectsPASS.Clear();
    }

    //This is where all the "On Collision Enter" functions should be handled from
    private void CheckCollisionActions(CollisionHull2D a, CollisionHull2D b)
    {
        if(a.gameObject.tag == "Bullet" && b.gameObject.tag == "Asteroid" || a.gameObject.tag == "Asteroid" && b.gameObject.tag == "Bullet")
        {
            if (a.gameObject.tag == "Asteroid" && !a.GetComponent<AsteroidScript>().canDestroy)
            {
                a.GetComponent<AsteroidScript>().canDestroy = true;
                a.GetComponent<MeshRenderer>().material = collisionMat;
                StartCoroutine(DestroyObjectsLater(b));
            }
            else if (b.gameObject.tag == "Asteroid" && !b.GetComponent<AsteroidScript>().canDestroy)
            {
                b.GetComponent<AsteroidScript>().canDestroy = true;
                b.GetComponent<MeshRenderer>().material = collisionMat;
                StartCoroutine(DestroyObjectsLater(a));
            }
            else
            {
                gmScript.SetScore(++gmScript.score);
                GameObject tempPS = Instantiate(ps, a.transform.position, Quaternion.identity);
                Destroy(tempPS, 20);
                GameObject tempAudio = Instantiate(new GameObject(), transform.position, Quaternion.identity);
                tempAudio.AddComponent<AudioSource>();
                AudioSource _tempAS = tempAudio.GetComponent<AudioSource>();
                _tempAS.volume = 0.3f;
                _tempAS.loop = false;
                _tempAS.PlayOneShot(wow);
                Destroy(tempAudio, 2);
                StartCoroutine(DestroyObjectsLater(a));
                StartCoroutine(DestroyObjectsLater(b));
            }
        }

        if(a.gameObject.tag == "Spaceship" && b.gameObject.tag == "Asteroid" || a.gameObject.tag == "Asteroid" && b.gameObject.tag == "Spaceship")
        {
            if(a.gameObject.tag == "Spaceship")
            {
                shakeFactor *= b.gameObject.GetComponent<CircleCollisionHull2D>().radius;
                StartCoroutine(DestroyObjectsLater(b));
                StartCoroutine(ShakeCounter()); //Apply shake
                StartCoroutine(HitShipColorChange(a.gameObject));
            }
            else
            {
                shakeFactor *= a.gameObject.GetComponent<CircleCollisionHull2D>().radius;
                StartCoroutine(ShakeCounter()); //Apply shake
                StartCoroutine(DestroyObjectsLater(a));
                StartCoroutine(HitShipColorChange(b.gameObject));
            }

            gmScript.SetScore(--gmScript.score);
        }

        if (a.gameObject.tag == "Spaceship" && b.gameObject.tag == "Bullet" || a.gameObject.tag == "Bullet" && b.gameObject.tag == "Spaceship")
        {
            if (a.gameObject.tag == "Spaceship")
            {
                StartCoroutine(DestroyObjectsLater(b));
            }
            else
            {
                StartCoroutine(DestroyObjectsLater(a));
            }

            gmScript.SetScore(--gmScript.score);
        }

    }

    public void RemoveObject(CollisionHull2D temp)
    {
        _objectsToCheck.Remove(temp);
    }

    IEnumerator ShakeCounter()
    {
        doShake = true;
        yield return new WaitForSeconds(shakeDuration);
        doShake = false;
        shakeFactor = shakeReset;
        Camera.main.transform.position = startCameraPos;
    }

    public void CallShake(float duration, float factor)
    {
        shakeDuration = duration; //Set duration
        shakeFactor = factor; //Set how intense it is
        StartCoroutine(ShakeCounter()); //Apply shake
    }

    IEnumerator HitShipColorChange(GameObject temp)
    {
        temp.GetComponent<MeshRenderer>().material = collisionMat;
        if (!audio.isPlaying)
        {
            audio.PlayOneShot(shipHitClip);
        }
        yield return new WaitForSeconds(0.75f);
        temp.GetComponent<MeshRenderer>().material = startMat;
    }

    IEnumerator DestroyObjectsLater(CollisionHull2D temp)
    {
        yield return new WaitForEndOfFrame();
        _objectsToCheck.Remove(temp);

        if (temp != null)
        {
            Destroy(temp.gameObject);
        }
    }

    public void AddObject(CollisionHull2D obj)
    {
        _objectsToCheck.Add(obj);
    }

    private void SetCollisionVelocity(CollisionHull2D a, CollisionHull2D b, CollisionHull2D.Collision contact)
    {
        //Fix interpentration 
        FixInterpenetration(a, b, contact);

        /*
         * 1) Calculate the new separating velocity based on restitution 
         * 2) Get the total inverse mass
         * 3) Return if the total inverse mass is zero (both are static objects)
         * 4) Calculate the inpulse velocity based on the the total inverse mass 
         * 5) Set the velocities of each object
        */

        float newSepVelocity = -contact.separatingVelocity * contact.restitutionA;
        float deltaVelocity = newSepVelocity - contact.separatingVelocity;

        float totalInverseMass = a.GetParticle().GetInvMass() + b.GetParticle().GetInvMass();

        if (totalInverseMass <= 0) return;

        float impulse = deltaVelocity / totalInverseMass;

        Vector2 impulsePerIMass = contact.contact[0].normal * impulse;

        a.GetParticle().velocity = a.GetParticle().velocity + impulsePerIMass * a.GetParticle().GetInvMass();
        b.GetParticle().velocity = b.GetParticle().velocity + impulsePerIMass * -b.GetParticle().GetInvMass();
    }

    private void FixInterpenetration(CollisionHull2D a, CollisionHull2D b, CollisionHull2D.Collision contact)
    {
        /*
         * 1) Check if penetration is greater than 1. Return if false
         * 2) Check if either object can be moved. Return if false
         * 3) Get the contact normal, set normal to right if spawned ontop of each other
         * 4) Find the amount of penetration resolution per unit of inverse mass
         * 5) Calculate the movement based on mass 
         * 6) Move each object 
        */
        
        if (contact.penetration <= 0)
            return;
       
        float totalInverseMass = a.GetParticle().GetInvMass() + b.GetParticle().GetInvMass();

        if (totalInverseMass <= 0)
            return;

  
        Vector2 normal = contact.contact[0].normal == Vector2.zero ? Vector2.right : contact.contact[0].normal;

        Vector2 movePerIMass = normal * (contact.penetration / totalInverseMass);

        Vector2 particalAMovement = movePerIMass * a.GetParticle().GetInvMass();
        Vector2 particalBMovement = movePerIMass * -b.GetParticle().GetInvMass();

        a.GetParticle().position = a.GetParticle().position - particalAMovement;
        b.GetParticle().position = b.GetParticle().position - particalBMovement;
    }


}
