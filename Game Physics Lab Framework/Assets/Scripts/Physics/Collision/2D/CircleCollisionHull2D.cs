﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine; 

public class CircleCollisionHull2D : CollisionHull2D
{
    public CircleCollisionHull2D() : base(CollisionHullType2D.hull_circle) { }

    [Range(0.0f, 100.0f)]
    public float radius;

    public override bool TestCollisionVsCircle(CircleCollisionHull2D other, ref Collision c)
    {
        // collision passes if distance between centers <= sum of radii
        // optimized colision passes if (distance between centers) squared <= (sum of radii) squared
        // 1. get the two centers
        // 2. difference between centers
        // 3. distance sqared = dot(diff, diff)
        // 4. sum of radii
        // 5. square sum
        // 6. DO THE TEST: distSq <= sumSq

        Vector3 distance = other.gameObject.transform.position - gameObject.transform.position;
        float radiiSum = other.radius + radius;
        if (Vector3.Dot(distance, distance) <= radiiSum * radiiSum)
        {
            c.a = this;
            c.b = other;

            /*
             *  1) Get the closest contact point
             *  2) Get the normal of collision 
             *  3) set the restitution of each object 
             *  4) Set the contact count 
             *  5) Set the separating velocity 
             *  6) Get the penetration depth 
            */

            c.contact[0].point = distance.normalized * radius + gameObject.transform.position;
            c.contact[0].normal = distance.normalized;
            c.restitutionA = restitution;
            c.restitutionB = other.restitution;
            c.contactCount = 1;
            c.separatingVelocity = Vector3.Dot(((Vector3)particle.velocity - (Vector3)other.particle.velocity), c.contact[0].normal);
            c.penetration = radiiSum - (other.transform.position - transform.position).magnitude;

            return true;
        }

        return false;
    }

    public override bool TestCollisionVsAABB(AxisAlignHull2D other, ref Collision c)
    {
        // calculate closest point by clamping circle center on each dimension
        // passes if closest point vs circle passes
        // 1. Get circle position
        // 2. Get AABB object position
        // 3. Clamp to nearest point on object
        // 4. Find difference of circlePos - objPos on each axis
        // 5. Compare distance of point to length of radius
        // 6. return results

        Vector3 relCenter = other.transform.InverseTransformPoint(transform.position);

        Vector3 closestPoint = Vector3.zero;
        float dist = relCenter.x;
        if (dist > other.half_dimensions.x) dist = other.half_dimensions.x;
        if (dist < -other.half_dimensions.x) dist = -other.half_dimensions.x;
        closestPoint.x = dist;

        dist = relCenter.y;
        if (dist > other.half_dimensions.y) dist = other.half_dimensions.y;
        if (dist < -other.half_dimensions.y) dist = -other.half_dimensions.y;
        closestPoint.y = dist;

        dist = (closestPoint - relCenter).sqrMagnitude;

        if(!(dist > radius * radius))
        {
            /*
            *  1) Get the closest contact point
            *  2) Get the normal of collision 
            *  3) set the restitution of each object 
            *  4) Set the contact count 
            *  5) Set the separating velocity 
            *  6) Get the penetration depth 
           */

            c.a = this;
            c.b = other;

            float clampedX = Mathf.Clamp(transform.position.x, other.bottomLeft.x, other.bottomRight.x);
            float clampedY = Mathf.Clamp(transform.position.y, other.bottomLeft.y, other.topLeft.y);

            Vector2 contactPoint = new Vector2(clampedX, clampedY);
            Vector2 surfaceNormal = ((Vector2)transform.position - contactPoint).normalized;

            c.contact[0].point = contactPoint;
            c.contact[0].normal = new Vector2(Mathf.Round(surfaceNormal.x), Mathf.Round(surfaceNormal.y));
            c.restitutionA = restitution;
            c.contactCount = 1;
            c.restitutionB = other.restitution;
            c.separatingVelocity = Vector3.Dot(((Vector3)other.GetParticle().velocity - (Vector3)particle.velocity), c.contact[0].normal);

            c.penetration = radius - ((Vector2)transform.position - contactPoint).magnitude;
            return true;
        }

        return false;
       
    }

    public override bool TestCollisionVsOBB(ObjectBoxHull2D other, ref Collision c)
    {
        // calculate closest point by clamping circle center on each dimension
        // passes if closest point vs circle passes
        // 1. Get circle position
        // 2. Get AABB object position
        // 3. Clamp to nearest point on object
        // 4. Find difference of circlePos - objPos on each axis
        // 5. Compare distance of point to length of radius
        // 6. return results


        Vector3 relCenter = other.transform.InverseTransformPoint(transform.position);

        Vector3 closestPoint = Vector3.zero;
        float dist = relCenter.x;
        if (dist > other.half_dimensions.x) dist = other.half_dimensions.x;
        if (dist < -other.half_dimensions.x) dist = -other.half_dimensions.x;
        closestPoint.x = dist;

        dist = relCenter.y;
        if (dist > other.half_dimensions.y) dist = other.half_dimensions.y;
        if (dist < -other.half_dimensions.y) dist = -other.half_dimensions.y;
        closestPoint.y = dist;

        dist = (closestPoint - relCenter).sqrMagnitude;

        if (!(dist > radius * radius))
        {

           /*
            *  1) Get the closest contact point
            *  2) Get the normal of collision 
            *  3) set the restitution of each object 
            *  4) Set the contact count 
            *  5) Set the separating velocity 
            *  6) Get the penetration depth 
           */


            c.a = this;
            c.b = other;

            float clampedX = Mathf.Clamp(transform.position.x, other.bottomLeft.x, other.bottomRight.x);
            float clampedY = Mathf.Clamp(transform.position.y, other.bottomLeft.y, other.topLeft.y);

            Vector2 contactPoint = new Vector2(clampedX, clampedY);
            Vector2 surfaceNormal = ((Vector2)transform.position - contactPoint).normalized;

            c.contact[0].point = contactPoint; //THIS IS WRONG FIX THIS LINE (CONTACT POINT IS NOT IN OBB SPACE)
            c.contact[0].normal = new Vector2(surfaceNormal.x, surfaceNormal.y);
            c.restitutionA = restitution;
            c.contactCount = 1;
            c.restitutionB = other.restitution;
            c.separatingVelocity = Vector3.Dot(((Vector3)other.GetParticle().velocity - (Vector3)particle.velocity), c.contact[0].normal);

            return true;
        }

        return false;
    }
}
