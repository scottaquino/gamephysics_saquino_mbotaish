﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class CollisionHull2D : MonoBehaviour
{
    public class Collision
    {
        public struct Contact
        {
            public Vector2 point;
            public Vector2 normal;            
        }

        public float restitutionA, restitutionB;
        public CollisionHull2D a = null, b = null;
        public Contact[] contact = new Contact[4];
        public int contactCount = 0;
        public bool status = false;

        public float separatingVelocity;
        public float penetration;
    }

    public float restitution = 1;

    public enum CollisionHullType2D
    {
        hull_circle,
        hull_aabb,
        hull_obb,
    }
    private CollisionHullType2D type { get; }

    protected CollisionHull2D(CollisionHullType2D type_set)
    {
        type = type_set;
    }

    protected Particle2D particle;
    public Particle2D GetParticle() { return particle; }

    // Start is called before the first frame update
    virtual protected void Start()
    {
        particle = GetComponent<Particle2D>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnDestroy()
    {
        CollisionManager.instance.RemoveObject(this);
    }

    public static bool TestCollision(CollisionHull2D a, CollisionHull2D b, ref Collision c)
    {
        c.status = true;

        if (a.type == CollisionHullType2D.hull_circle && b.type == CollisionHullType2D.hull_circle)
            return a.TestCollisionVsCircle(b as CircleCollisionHull2D, ref c);

        if (a.type == CollisionHullType2D.hull_circle && b.type == CollisionHullType2D.hull_aabb)
            return a.TestCollisionVsAABB(b as AxisAlignHull2D, ref c);

        if (a.type == CollisionHullType2D.hull_circle && b.type == CollisionHullType2D.hull_obb)
            return a.TestCollisionVsOBB(b as ObjectBoxHull2D, ref c);

        if (a.type == CollisionHullType2D.hull_aabb && b.type == CollisionHullType2D.hull_circle)
            return a.TestCollisionVsCircle(b as CircleCollisionHull2D, ref c);

        if (a.type == CollisionHullType2D.hull_aabb && b.type == CollisionHullType2D.hull_aabb)
            return a.TestCollisionVsAABB(b as AxisAlignHull2D, ref c);

        if (a.type == CollisionHullType2D.hull_aabb && b.type == CollisionHullType2D.hull_obb)
            return a.TestCollisionVsOBB(b as ObjectBoxHull2D, ref c);

        if (a.type == CollisionHullType2D.hull_obb && b.type == CollisionHullType2D.hull_circle)
            return a.TestCollisionVsCircle(b as CircleCollisionHull2D, ref c);

        if (a.type == CollisionHullType2D.hull_obb && b.type == CollisionHullType2D.hull_aabb)
            return a.TestCollisionVsAABB(b as AxisAlignHull2D, ref c);

        if (a.type == CollisionHullType2D.hull_obb && b.type == CollisionHullType2D.hull_obb)
            return a.TestCollisionVsOBB(b as ObjectBoxHull2D, ref c);

        c.status = false;

        return false;
    }

    public static bool Vector2Comparison(Vector2 left, Vector2 right)
    {
        if (left.x >= right.x && left.y >= right.y)
        {
            return true;
        }
        return false;
    }

    public abstract bool TestCollisionVsCircle(CircleCollisionHull2D other, ref Collision c);

    public abstract bool TestCollisionVsAABB(AxisAlignHull2D other, ref Collision c);

    public abstract bool TestCollisionVsOBB(ObjectBoxHull2D other, ref Collision c);
}
