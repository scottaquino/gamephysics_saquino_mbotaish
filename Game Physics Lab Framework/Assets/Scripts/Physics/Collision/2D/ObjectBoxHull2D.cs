﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectBoxHull2D : CollisionHull2D
{
    public ObjectBoxHull2D() : base(CollisionHullType2D.hull_obb) { }

    public Vector2 dimensions;

    public Vector3 topLeft;
    public Vector3 topRight;
    public Vector3 bottomLeft;
    public Vector3 bottomRight;
    public Vector3 half_dimensions;

    public Matrix4x4 transMaxtrix;
    // Start is called before the first frame update
    protected override void Start()
    {
        base.Start();
        half_dimensions = dimensions * 0.5f;
        UpdateBounds();
    }

    // Update is called once per frame
    void Update()
    {
        UpdateBounds();
    }   

    public void UpdateBounds()
    {
        transMaxtrix = transform.localToWorldMatrix;
        bottomLeft = new Vector3(-half_dimensions.x, -half_dimensions.y);
        topRight = new Vector3(half_dimensions.x, half_dimensions.y);
        topLeft = new Vector3(bottomLeft.x, topRight.y);
        bottomRight = new Vector3(topRight.x, bottomLeft.y);

        topLeft = transMaxtrix.MultiplyPoint(topLeft);
        topRight = transMaxtrix.MultiplyPoint(topRight);
        bottomLeft = transMaxtrix.MultiplyPoint(bottomLeft); 
        bottomRight = transMaxtrix.MultiplyPoint(bottomRight); 
    }

    public override bool TestCollisionVsCircle(CircleCollisionHull2D other, ref Collision c)
    {
        // see circle
        // transform circle position by multiplying by box world matrix inverse
        // 1. Get inverse matrix
        // 2. Get reference to max and min values for OBB
        // 3. Get circle position in OBB space
        // 4. Get point clamped in bounds of OBB
        // 5. Calculate distance vector to test
        // 6. Compare distance vector to radius
        // 7. return results

        return other.TestCollisionVsOBB(this, ref c);
    }

    public override bool TestCollisionVsAABB(AxisAlignHull2D other, ref Collision c)
    {

        // see AABB

        // 1. calculate extents of boxes for AABB and OBB portion
        // 2. multiply by OBB inverse matrix
        // 3. test extents against AABB
        // 4. calculate new OBB using OBB inverse matrix
        // 5. test extents agains new OBB
        // 6. return results
        
        return other.TestCollisionVsOBB(this, ref c);
    }

    public override bool TestCollisionVsOBB(ObjectBoxHull2D other, ref Collision c)
    {
        // 1. Find all the axiis on one of the OBB object
        // 2. Project all points onto each axis for both OBB objects
        // 3. Find the min and max points of each OBB object
        // 3. Compare the min and maxs of each point
        // 4. If there is not over lap return false (no collision)
        // 5. Repeat steps 1-4 with the other OBB object axiis 
        // 6. If no test fails return true

        Vector2[] OBBAxis = { other.transform.right, other.transform.up, transform.right, transform.up };

        for (int i = 0; i < 4; i++)
        {
            Vector3 otherProjectTL = Vector3.Project(other.topLeft, OBBAxis[i]);
            Vector3 otherProjectTR = Vector3.Project(other.topRight, OBBAxis[i]);
            Vector3 otherProjectBL = Vector3.Project(other.bottomLeft, OBBAxis[i]);
            Vector3 otherProjectBR = Vector3.Project(other.bottomRight, OBBAxis[i]);

            Vector3 ProjectTL = Vector3.Project(topLeft, OBBAxis[i]);
            Vector3 ProjectTR = Vector3.Project(topRight, OBBAxis[i]);
            Vector3 ProjectBL = Vector3.Project(bottomLeft, OBBAxis[i]);
            Vector3 ProjectBR = Vector3.Project(bottomRight, OBBAxis[i]);

            Vector2 maxOBB = new Vector2(Mathf.Max(otherProjectTL.x, otherProjectTR.x, otherProjectBL.x, otherProjectBR.x), Mathf.Max(otherProjectTL.y, otherProjectTR.y, otherProjectBL.y, otherProjectBR.y));
            Vector2 minOBB = new Vector2(Mathf.Min(otherProjectTL.x, otherProjectTR.x, otherProjectBL.x, otherProjectBR.x), Mathf.Min(otherProjectTL.y, otherProjectTR.y, otherProjectBL.y, otherProjectBR.y));

            Vector2 maxABB = new Vector2(Mathf.Max(ProjectTL.x, ProjectTR.x, ProjectBL.x, ProjectBR.x), Mathf.Max(ProjectTL.y, ProjectTR.y, ProjectBL.y, ProjectBR.y));
            Vector2 minABB = new Vector2(Mathf.Min(ProjectTL.x, ProjectTR.x, ProjectBL.x, ProjectBR.x), Mathf.Min(ProjectTL.y, ProjectTR.y, ProjectBL.y, ProjectBR.y));

            if ((!(Vector2Comparison(maxABB, minOBB) && Vector2Comparison(maxOBB, minABB))))
                return false;
        }

        return true;
    }
}
