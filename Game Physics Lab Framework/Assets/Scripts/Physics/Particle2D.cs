﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Particle2D : MonoBehaviour
{
    // Lab 1: Step 1
    public Vector2 position, velocity, acceleration;
    public float rotation = 0, angularVelocity = 0, angularAcceleration = 0;

    // Lab 2: Step 1
    public GameObject anchorPrefab;
    public float startingMass = 1;
    public float maxSpeed = 10;
    public float maxRotSpeed = 1;
    private float mass, massInv;
    public bool isStatic = false;

    [Header("Friction")]
    public float frictionCoefficient_static;
    public float frictionCoefficient_kinetic;

    [Header("Drag")]
    public float fluidDensity;
    public Vector2 fluidVelocity;
    public float area;
    public float dragCoefficient;

    [Header("Springs")]   
    public Vector2 relativeAnchorPos;
    public float springRestingLength;
    public float springStiffnessCoefficient;
    private Vector2 anchorPos;

    private enum Lab2Test { FREEFALL, SIDEWAYS, SLIDING, SPRING }
    private enum Lab2Resistance { NONE, FRICTION, DRAG, COMBINED }
    private enum Lab2ForceType { CONSTANT, IMPULSE }
    private Lab2Test lab2TestCase;
    private Lab2Resistance lab2TestResistance;
    private Lab2ForceType lab2ForceType = Lab2ForceType.CONSTANT;

    // Lab 1: Bonus
    private enum positionMethod { EULER, KINEMATIC, CIRCLE };
    private enum rotationMethod { EULER, KINEMATIC };
    private positionMethod posMeth;
    private rotationMethod rotMeth;
    private float startTime = 0;
    private Vector3 startPos = new Vector3();
    private int amp = 1;
    private int frequency = 1;

    // Lab 2: Step 2
    private Vector2 force;

    //Lab 3: Step 1
   
    [Header("Torque")]
    public float torque;
    public Vector2 centerOfMassLocal;
    private Vector2 centerOfMassWorld;
    public Vector2 pointOfForce;
    private float inverseMomentOfIneria;
    private float momentOfInertia = 1;
    

    [Header("Disk Inertia")]
    public float diskRadius = 1;

    [Header("Ring Inertia")]
    public float innerRadius = 1;
    public float outerRadius = 1;

    [Header("Rectangle Inertia")]
    public float xDimension = 1;
    public float yDimension = 1;

    [Header("Line Inertia")]
    public float length = 1;

    public enum objectType { DISK, RING, BOX, LINE };
    public objectType oType { get; set; }


    // Start is called before the first frame update
    void Start()
    {
        //Set default starting values
        startPos = transform.position;
        position = startPos;
        
        startTime = Time.fixedTime;

        //Lab 1
        SetMass(startingMass);
        SetEuler();
        // //Lab 2
        // anchorPos = position + relativeAnchorPos;
        // if (lab2ForceType == Lab2ForceType.IMPULSE)
        //     InitialPush();

        //Lab 3
        // Instantiate(anchorPrefab, centerOfMassWorld, Quaternion.identity); //Show center of mass relative to object

        centerOfMassWorld = (centerOfMassLocal + (Vector2)transform.position);
        pointOfForce += centerOfMassWorld;

    }   

    #region Lab 1
    void UpdateLab1PosRot()
    {
        // Lab 1: Step 3
        // Integrate based on chosen method
        switch (posMeth)
        {
            case positionMethod.EULER:
                UpdatePositionExplicitEuler(Time.fixedDeltaTime);
                break;
            case positionMethod.KINEMATIC:
                UpdatePositionKinematic(Time.fixedDeltaTime);
                break;
            case positionMethod.CIRCLE:
                UpdatePositionKinematic(Time.fixedDeltaTime);
                break;
            default:
                UpdatePositionExplicitEuler(Time.fixedDeltaTime);
                break;
        }

        //Rotate based on chosen method
        switch (rotMeth)
        {
            case rotationMethod.EULER:
                UpdateRotationEulerExplicit(Time.fixedDeltaTime);
                break;
            case rotationMethod.KINEMATIC:
                UpdateRotationKinematic(Time.fixedDeltaTime);
                break;
            default:
                UpdateRotationEulerExplicit(Time.fixedDeltaTime);
                break;
        }

        //angularAcceleration = amp * -Mathf.Sin((Time.fixedTime - startTime) * frequency);
    }

    void TestLab1()
    {
        if (isStatic)
            return;

        // Lab 1: Step 4
        // Test - Decides path of movement based on chosen condition
        switch (posMeth)
        {
            case positionMethod.EULER:
                acceleration.x = amp * -Mathf.Sin((Time.fixedTime - startTime) * frequency);
                //transform.eulerAngles = new Vector3(0f, 0f, Mathf.Rad2Deg * rotation);
                break;
            case positionMethod.KINEMATIC:
                acceleration.y = amp * -Mathf.Sin((Time.fixedTime - startTime) * frequency);
                //transform.eulerAngles = new Vector3(Mathf.Rad2Deg * rotation, 0f, 0f);
                break;
            case positionMethod.CIRCLE:
                acceleration.x = amp * -Mathf.Sin((Time.fixedTime - startTime) * frequency);
                acceleration.y = amp * Mathf.Cos((Time.fixedTime - startTime) * frequency);
               // transform.eulerAngles = new Vector3(Mathf.Rad2Deg * rotation, Mathf.Rad2Deg * rotation, 0f);
                break;
        }
    }

    //Reset starting values for EULER movement and rotation method
    public void SetEuler()
    {
        posMeth = positionMethod.EULER;
        rotMeth = rotationMethod.EULER;
    }

    //Reset starting values for KINEMATIC movement and rotation
    public void SetKinematic()
    {
        posMeth = positionMethod.KINEMATIC;
        rotMeth = rotationMethod.KINEMATIC;
    }

    //Reset starting values for KINEMATIC circular movement with KINEMATIC rotation
    public void SetCircle()
    {
        posMeth = positionMethod.CIRCLE;
        rotMeth = rotationMethod.KINEMATIC;
    }

    // Step 2
    void UpdatePositionExplicitEuler(float dt)
    {
        // x(t+dt) = x(t) + v(t)dt
        // Euler's method:
        // F(t+dt) = F(t) + f(t)dt
        //                + (dF/dt)dt

        position += velocity * dt;

        // Update velocity
        // v(t+dt) = v(t) + a(t)dt
        velocity += acceleration * dt;
    }

    void UpdatePositionKinematic(float dt)
    {
        // x(t+dt) = x(t) + v(t)dt + (0.5 * a(t)(dt * dt))
        position += (velocity * dt) + (0.5f * acceleration * (dt * dt));

        // Update velocity
        // v(t+dt) = v(t) + a(t)dt
        velocity += acceleration * dt;
    }

    void UpdateRotationEulerExplicit(float dt)
    {
        // x(t+dt) = x(t) + v(t)dt
        // Euler's method:
        // F(t+dt) = F(t) + f(t)dt
        //                + (dF/dt)dt
        rotation += angularVelocity * dt;
        rotation %= 360;

        // Update velocity
        // v(t+dt) = v(t) + a(t)dt
        angularVelocity += angularAcceleration * dt;
    }

    void UpdateRotationKinematic(float dt)
    {
        // x(t+dt) = x(t) + v(t)dt + (0.5 * a(t)(dt * dt))
        rotation += (angularVelocity * dt) + (0.5f * angularAcceleration * (dt * dt));
        rotation %= 360;

        // Update velocity
        // v(t+dt) = v(t) + a(t)dt
        angularVelocity += angularAcceleration * dt;
    }
    #endregion

    #region Lab2
    private void UpdateForces()
    {
        Vector2 f_gravity = mass * new Vector2(0.0f, -9.8f);
        Vector2 f_testForce = mass * new Vector2(2.0f, 0.0f); //USED TO SIMULATE SIDEWAYS MOTION

        Vector2 f_normal = Vector3.zero;

        switch (lab2TestCase)
        {
            case Lab2Test.FREEFALL:
                if (lab2ForceType == Lab2ForceType.CONSTANT)
                    AddForce(ForceGenerator.GenerateForce_Gravity(mass, -9.8f, Vector2.up)); //GRAVITY IS GOOD
                f_normal = ForceGenerator.GenerateForce_Normal(-f_gravity, Vector2.up);
                break;
            case Lab2Test.SIDEWAYS:
                if (lab2ForceType == Lab2ForceType.CONSTANT)
                    AddForce(f_testForce);
                f_normal = ForceGenerator.GenerateForce_Normal(-f_testForce, Vector2.right);
                break;
            case Lab2Test.SLIDING:
                //SLIDING FORCE GIVEN A NORMAL
                f_normal = ForceGenerator.GenerateForce_Normal(f_gravity, (-Vector3.right + Vector3.down).normalized);
                Vector2 f_sliding = ForceGenerator.GenerateForce_Sliding(f_gravity, f_normal);
                AddForce(f_sliding);
                break;
            case Lab2Test.SPRING:
                if (lab2ForceType == Lab2ForceType.CONSTANT)
                    AddForce(ForceGenerator.GenerateForce_Gravity(mass, -9.8f, Vector2.up));
                AddForce(ForceGenerator.GenerateForce_Spring(position, anchorPos, springRestingLength, springStiffnessCoefficient)); //MAKE THIS STUFF EDITABLE IN UI
                break;
        }

        switch (lab2TestResistance)
        {
            case Lab2Resistance.FRICTION:
                if (velocity.magnitude == 0)
                    AddForce(ForceGenerator.GenerateForce_Friction_Static(f_normal, -force, frictionCoefficient_static)); //Static Friction
                else
                    AddForce(ForceGenerator.GenerateForce_Friction_Kinetic(f_normal, velocity, frictionCoefficient_kinetic)); //Kinetic Friction
                break;
            case Lab2Resistance.DRAG:
                AddForce(ForceGenerator.GenerateForce_Drag(velocity, fluidVelocity, fluidDensity, area, dragCoefficient));
                break;
            case Lab2Resistance.COMBINED:
                if (velocity.magnitude == 0)
                    AddForce(ForceGenerator.GenerateForce_Friction_Static(f_normal, -force, frictionCoefficient_static)); //Static Friction
                else
                    AddForce(ForceGenerator.GenerateForce_Friction_Kinetic(f_normal, velocity, frictionCoefficient_kinetic)); //Kinetic Friction

                AddForce(ForceGenerator.GenerateForce_Drag(velocity, fluidVelocity, fluidDensity, area, dragCoefficient));
                break;
            case Lab2Resistance.NONE:
                //Do nothing :)
                break;
        }
    }


    private void InitialPush()
    {
        // Lab 2: Test
        AddForce(mass * Vector2.right * 100.0f);
    }

    public void AddForce(Vector2 newForce)
    {
        // D'Alembert
        force += newForce;
    }

    void UpdateAcceleration()
    {
        // Newton2
        acceleration = massInv * force;
        force.Set(0.0f, 0.0f);
    }

    #endregion

    #region Lab3

    public void AddTorque(Vector2 force)
    {
        Vector2 momentArm = pointOfForce - centerOfMassWorld;
        torque += (momentArm.x * force.y) - (momentArm.y * force.x);
    }

    private void UpdateAngularAcceleration()
    {
        angularAcceleration = (1 / momentOfInertia) * torque; //a = invers I * torque
        torque = 0.0f; //Reset torque for next frame
    }

    private void UpdateRotation()
    {
        //print(rotation + ":" + angularVelocity);
        if(rotation != 0)
            transform.eulerAngles = new Vector3( 0f, 0f, Mathf.Rad2Deg * rotation);
    }

    void CalculateTorque_Disk()
    {
        momentOfInertia = 0.5f * mass * (diskRadius * diskRadius);
    }

    void CalculateTorque_Ring()
    {
        momentOfInertia = 0.5f * mass * ((outerRadius * outerRadius) + (innerRadius * innerRadius));
    }

    void CalculateTorque_Rectangle()
    {
        momentOfInertia = (1f / 12f) * mass * ((xDimension * xDimension) + (yDimension * yDimension));
    }

    void CalculateTorque_Line()
    {
        momentOfInertia = (1f / 12f) * mass * length;
    }

    #endregion

    // Update is called once per frame
    void FixedUpdate()
    {
        UpdateLab1PosRot(); // Lab 1 fun

        UpdateAcceleration();

        // Apply to transform
        transform.position = position;
      

        //Update rotation based on calculated values above
        UpdateAngularAcceleration();
        UpdateRotation();

        if(velocity.magnitude >= maxSpeed)
        {
            velocity = velocity.normalized * maxSpeed;
        }

        if(Mathf.Abs(angularVelocity) >= maxRotSpeed)
        {
            if (angularVelocity > 0)
            {
                angularVelocity = maxRotSpeed;
            }
            else
            {
                angularVelocity = -maxRotSpeed;
            }
        }

    }

    public void GetTorqueInput()
    {
        if(Input.GetKey(KeyCode.D))
        {
            AddTorque(new Vector2(0, -10));
        }

        if (Input.GetKey(KeyCode.A))
        {
            AddTorque(new Vector2(0, 10));
        }
    }

    public void SetFreeFall() { lab2TestCase = Lab2Test.FREEFALL; }
    public void SetSideways() { lab2TestCase = Lab2Test.SIDEWAYS; }
    public void SetCombined() { lab2TestCase = Lab2Test.SLIDING; }
    public void SetSpring() { lab2TestCase = Lab2Test.SPRING; }
    public void SetResistanceFriction() { lab2TestResistance = Lab2Resistance.FRICTION; }
    public void SetResistanceDrag() { lab2TestResistance = Lab2Resistance.DRAG; }
    public void SetResistanceCombined() { lab2TestResistance = Lab2Resistance.COMBINED; }
    public void SetResistanceNone() { lab2TestResistance = Lab2Resistance.NONE; }
    public void SetForceTypeToImpulse() { lab2ForceType = Lab2ForceType.IMPULSE; }
    public void SetAmp(int tempAmp) { amp = tempAmp; }
    public void SetFrequency(int tempFreq) { frequency = tempFreq; }
    public void SetMass(float newMass)
    {
        mass = startingMass > 0.0f ? newMass : 0.0f; // Same as Mathf.Max(0.0f, newMass);
        massInv = mass > 0.0f ? 1.0f / mass : 0.0f;
    }

    public string GetSetting() { return posMeth.ToString(); }
    public float GetMass() { return mass; }
    public float GetInvMass() { return massInv; }
    public int GetAmp() { return amp; }
    public int GetFrequency() { return frequency; }
    public float GetTorque() { return torque; }
    public Vector2 GetForce() { return force; }

    public void SetInertiaDisk() { oType = objectType.DISK; }
    public void SetInertiaRing() { oType = objectType.RING; }
    public void SetInertiaBox() { oType = objectType.BOX; }
    public void SetInertiaLine() { oType = objectType.LINE; }
    public void SetTorqueForce(float newT) { torque = newT; }
    public void SetDiskRadius(float diskRad) { diskRadius = diskRad; }
    public void SetInnerRadius(float inner) { innerRadius = inner; }
    public void SetOuterRadius(float outer) { innerRadius = outer; }
    public void SetBoxWidth(float width) { xDimension = width; }
    public void SetBoxHeight(float height) { yDimension = height; }
    public void SetLineLength(float lineLength) { length = lineLength; }

}
