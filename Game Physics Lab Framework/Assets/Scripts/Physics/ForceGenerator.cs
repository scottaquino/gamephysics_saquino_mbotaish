﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ForceGenerator
{
    // Lab 2: Step 3
    public static Vector3 GenerateForce_Gravity(float particleMass, float gravitationalConstant, Vector3 worldUp)
    {
        // f = mg
        Vector3 f_gravity = particleMass * gravitationalConstant * worldUp;
        return f_gravity;
    }

    public static Vector3 GenerateForce_Normal(Vector3 f_gravity, Vector3 surfaceNormal_unit)
    {
        // f_normal = proj(f_gravity, surfaceNormal_unit)
        Vector3 f_normal = Vector3.Project(-f_gravity, surfaceNormal_unit);
        return f_normal;
    }

    public static Vector3 GenerateForce_Sliding(Vector3 f_gravity, Vector3 f_normal)
    {
        // f_sliding = f_gravity + f_normal
        Vector3 f_sliding = f_gravity + f_normal;
        return f_sliding;
    }

    public static Vector3 GenerateForce_Friction_Static(Vector3 f_normal, Vector3 f_opposing, float frictionCoefficient_static)
    {
        // f_friction_s = -f_opposing if less than max, else -coeff*f_normal (max amount is coeff*|f_normal|)
        float maxFriction_s = frictionCoefficient_static * f_normal.magnitude;
        Vector3 f_friction_s = f_opposing.magnitude < maxFriction_s ? f_opposing : -frictionCoefficient_static * f_normal;
        return f_friction_s;
    }

    public static Vector3 GenerateForce_Friction_Kinetic(Vector3 f_normal, Vector3 particleVelocity, float frictionCoefficient_kinetic)
    {
        // f_friction_k = -coeff*|f_normal| * unit(vel)
        Vector3 f_friction_k = -frictionCoefficient_kinetic * f_normal.magnitude * Vector3.Normalize(particleVelocity);
        return f_friction_k;
    }

    public static Vector3 GenerateForce_Drag(Vector3 particleVelocity, Vector3 fluidVelocity, float fluidDensity, float objectArea_crossSection, float objectDragCoefficient)
    {
        // f_drag = (p * u^2 * area * coeff)/2
        Vector3 f_drag = (fluidDensity * ((fluidVelocity - particleVelocity).magnitude * (fluidVelocity - particleVelocity)) * objectArea_crossSection * objectDragCoefficient) * 0.5f;
        return f_drag;
    }

    public static Vector3 GenerateForce_Spring(Vector3 particlePosition, Vector3 anchorPosition, float springRestingLength, float springStiffnessCoefficient)
    {
        Vector3 sprintDir = (particlePosition - anchorPosition);
        float springLength = sprintDir.magnitude;
        // f_spring = -coeff*(spring length - spring resting length)
        Vector3 f_spring = (sprintDir * - springStiffnessCoefficient * (springLength - springRestingLength)) / springLength;
        return f_spring;
    }
}
