﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InputController : MonoBehaviour
{
    public float mainThrusterSpeed;
    public float reverseThrusterSpeed;
    public float sideThrusterSpeed;
    public float shotForce;
    public float shotDelay;

    [Header("Grenade Info")]
    public int grenadeCount = 5;
    public float lifeLength = 2.0f;
    public Text grenadeCountText;
    public GameObject grenadePrefab;
    public float grenadeForce;

    [Header("------")]
    [SerializeField] GameObject bullet = null;
    [SerializeField] Transform bulletSpawn = null;

    [SerializeField] Material activeMat = null;
    [SerializeField] Material defaultMat = null;

    [SerializeField] AudioClip shootSound = null;

    private List<MeshRenderer> thrusterObjects = new List<MeshRenderer>();
    private AudioSource audio;
    bool canShoot = true;
    Particle2D particle;

    // Start is called before the first frame update
    void Start()
    {
        audio = GetComponent<AudioSource>();
        grenadeCountText.text = grenadeCount.ToString();
        particle = GetComponent<Particle2D>();

        foreach(Transform child in transform)
        {
            thrusterObjects.Add(child.gameObject.GetComponent<MeshRenderer>());
        }
    }

    // Update is called once per frame
    void Update()
    {
        CheckInput();
        CheckBounds();
    }

    void CheckBounds()
    {
        if(transform.position.x >= 13f)
        {
            //Out of bounds of right
            transform.position = new Vector3(-12f, transform.position.y, transform.position.z);
            particle.position = transform.position;
        }
        if (transform.position.x <= -13f)
        {
            //Out of bounds of left
            transform.position = new Vector3(12f, transform.position.y, transform.position.z);
            particle.position = transform.position;
        }
        if (transform.position.y >= 8.5f)
        {
            //Out of bounds of top
            transform.position = new Vector3(transform.position.x, -7f, transform.position.z);
            particle.position = transform.position;
        }
        if (transform.position.y <= -7.5f)
        {
            //Out of bounds of bottom
            transform.position = new Vector3(transform.position.x, 8f, transform.position.z);
            particle.position = transform.position;
        }
    }

    IEnumerator ShotDelay()
    {
        canShoot = false;
        yield return new WaitForSeconds(shotDelay);
        canShoot = true;
    }

    void CheckInput()
    {
        //Shooting
        if (Input.GetMouseButton(0) && canShoot)
        {
            GameObject temp = Instantiate(bullet, bulletSpawn.transform.position, Quaternion.identity);
            temp.GetComponent<Particle2D>().AddForce(transform.up * shotForce);
            Destroy(temp, 2.5f);
            audio.PlayOneShot(shootSound);
            CollisionManager.instance.AddObject(temp.GetComponent<CircleCollisionHull2D>());
            StartCoroutine(ShotDelay());
        }

        if (Input.GetMouseButtonDown(1) && grenadeCount > 0 && !GameManagerScript.instance.IsActiveGrenadeLive())
        {
            GameObject temp = Instantiate(grenadePrefab, bulletSpawn.transform.position, Quaternion.identity);
            temp.GetComponent<Particle2D>().AddForce(transform.up * grenadeForce);
            temp.GetComponent<Particle2D>().AddTorque(Vector2.right * 550.0f);
            StartCoroutine(GameManagerScript.instance.GRENADE_GET_DOWN(temp));
            Destroy(temp, lifeLength);
            grenadeCount--;
            grenadeCountText.text = grenadeCount.ToString();
        }

        //--------------------------------------------On key down-----------------------------------------//
        if (Input.GetKey(KeyCode.W))
        {
            //Thruster forward (Bottom)
            particle.AddForce(mainThrusterSpeed * transform.up);
            thrusterObjects[0].material = activeMat;
        }
        if (Input.GetKey(KeyCode.A))
        {
            //Thruster left lean (Right) TORQUE
            particle.AddTorque(new Vector2(0, sideThrusterSpeed));
            thrusterObjects[2].material = activeMat;
        }
        if (Input.GetKey(KeyCode.S))
        {
            //Thruster reverse (Top)
            particle.AddForce(reverseThrusterSpeed * -transform.up);
            thrusterObjects[1].material = activeMat;
        }
        if (Input.GetKey(KeyCode.D))
        {
            //Thruster right lean (Left) TORQUE
            particle.AddTorque(new Vector2(0, -sideThrusterSpeed));
            thrusterObjects[3].material = activeMat;
        }

        //--------------------------------------------On key up----------------------------------------//
        if (Input.GetKeyUp(KeyCode.W))
        {
            //Thruster forward (Bottom)
            thrusterObjects[0].material = defaultMat;
        }
        if (Input.GetKeyUp(KeyCode.A))
        {
            //Thruster left lean (Right) TORQUE
            particle.angularVelocity = 0;
            particle.angularAcceleration = 0;
            thrusterObjects[2].material = defaultMat;
        }
        if (Input.GetKeyUp(KeyCode.S))
        {
            //Thruster reverse (Top)
            thrusterObjects[1].material = defaultMat;
        }
        if (Input.GetKeyUp(KeyCode.D))
        {
            //Thruster right lean (Left) TORQUE
            particle.angularVelocity = 0;
            particle.angularAcceleration = 0;
            thrusterObjects[3].material = defaultMat;
        }
    }
}
